// This is a generated file.  Don't edit it directly!

using QuickFix.Fields;
namespace QuickFix
{
    namespace FIX42 
    {
        public class IB_AccountDataEnd : Message
        {
            public const string MsgType = "EB";

            public IB_AccountDataEnd() : base()
            {
                this.Header.SetField(new QuickFix.Fields.MsgType("EB"));
            }

            public IB_AccountDataEnd(
                    QuickFix.Fields.IB_ReqID aIB_ReqID
                ) : this()
            {
                this.IB_ReqID = aIB_ReqID;
            }

            public QuickFix.Fields.IB_ReqID IB_ReqID
            { 
                get 
                {
                    QuickFix.Fields.IB_ReqID val = new QuickFix.Fields.IB_ReqID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.IB_ReqID val) 
            { 
                this.IB_ReqID = val;
            }
            
            public QuickFix.Fields.IB_ReqID Get(QuickFix.Fields.IB_ReqID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.IB_ReqID val) 
            { 
                return IsSetIB_ReqID();
            }
            
            public bool IsSetIB_ReqID() 
            { 
                return IsSetField(Tags.IB_ReqID);
            }
        }
    }
}
