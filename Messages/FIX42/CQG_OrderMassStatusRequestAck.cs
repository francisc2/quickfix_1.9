// This is a generated file.  Don't edit it directly!

using QuickFix.Fields;
namespace QuickFix
{
    namespace FIX42 
    {
        public class CQG_OrderMassStatusRequestAck : Message
        {
            public const string MsgType = "UBR";

            public CQG_OrderMassStatusRequestAck() : base()
            {
                this.Header.SetField(new QuickFix.Fields.MsgType("UBR"));
            }

            public CQG_OrderMassStatusRequestAck(
                    QuickFix.Fields.CQG_ReqResult aCQG_ReqResult
                ) : this()
            {
                this.CQG_ReqResult = aCQG_ReqResult;
            }

            public QuickFix.Fields.CQG_MassStatusReqID CQG_MassStatusReqID
            { 
                get 
                {
                    QuickFix.Fields.CQG_MassStatusReqID val = new QuickFix.Fields.CQG_MassStatusReqID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_MassStatusReqID val) 
            { 
                this.CQG_MassStatusReqID = val;
            }
            
            public QuickFix.Fields.CQG_MassStatusReqID Get(QuickFix.Fields.CQG_MassStatusReqID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_MassStatusReqID val) 
            { 
                return IsSetCQG_MassStatusReqID();
            }
            
            public bool IsSetCQG_MassStatusReqID() 
            { 
                return IsSetField(Tags.CQG_MassStatusReqID);
            }
            public QuickFix.Fields.CQG_ReqResult CQG_ReqResult
            { 
                get 
                {
                    QuickFix.Fields.CQG_ReqResult val = new QuickFix.Fields.CQG_ReqResult();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_ReqResult val) 
            { 
                this.CQG_ReqResult = val;
            }
            
            public QuickFix.Fields.CQG_ReqResult Get(QuickFix.Fields.CQG_ReqResult val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_ReqResult val) 
            { 
                return IsSetCQG_ReqResult();
            }
            
            public bool IsSetCQG_ReqResult() 
            { 
                return IsSetField(Tags.CQG_ReqResult);
            }
            public QuickFix.Fields.CQG_NoAccounts CQG_NoAccounts
            { 
                get 
                {
                    QuickFix.Fields.CQG_NoAccounts val = new QuickFix.Fields.CQG_NoAccounts();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_NoAccounts val) 
            { 
                this.CQG_NoAccounts = val;
            }
            
            public QuickFix.Fields.CQG_NoAccounts Get(QuickFix.Fields.CQG_NoAccounts val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_NoAccounts val) 
            { 
                return IsSetCQG_NoAccounts();
            }
            
            public bool IsSetCQG_NoAccounts() 
            { 
                return IsSetField(Tags.CQG_NoAccounts);
            }
            public QuickFix.Fields.CQG_LastRptRequested CQG_LastRptRequested
            { 
                get 
                {
                    QuickFix.Fields.CQG_LastRptRequested val = new QuickFix.Fields.CQG_LastRptRequested();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_LastRptRequested val) 
            { 
                this.CQG_LastRptRequested = val;
            }
            
            public QuickFix.Fields.CQG_LastRptRequested Get(QuickFix.Fields.CQG_LastRptRequested val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_LastRptRequested val) 
            { 
                return IsSetCQG_LastRptRequested();
            }
            
            public bool IsSetCQG_LastRptRequested() 
            { 
                return IsSetField(Tags.CQG_LastRptRequested);
            }
            public class CQG_NoAccountsGroup : Group
            {
                public static int[] fieldOrder = {Tags.Account, 0};
            
                public CQG_NoAccountsGroup() 
                  :base( Tags.CQG_NoAccounts, Tags.Account, fieldOrder)
                {
                }
            
                public override Group Clone()
                {
                    var clone = new CQG_NoAccountsGroup();
                    clone.CopyStateFrom(this);
                    return clone;
                }
            
                public QuickFix.Fields.Account Account
                { 
                    get 
                    {
                        QuickFix.Fields.Account val = new QuickFix.Fields.Account();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.Account val) 
                { 
                    this.Account = val;
                }
                
                public QuickFix.Fields.Account Get(QuickFix.Fields.Account val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.Account val) 
                { 
                    return IsSetAccount();
                }
                
                public bool IsSetAccount() 
                { 
                    return IsSetField(Tags.Account);
                }
            
            }
        }
    }
}
