// This is a generated file.  Don't edit it directly!

using QuickFix.Fields;
namespace QuickFix
{
    namespace FIX42 
    {
        public class CQG_Automatic_Order_Update : Message
        {
            public const string MsgType = "UZV";

            public CQG_Automatic_Order_Update() : base()
            {
                this.Header.SetField(new QuickFix.Fields.MsgType("UZV"));
            }

            public CQG_Automatic_Order_Update(
                    QuickFix.Fields.Account aAccount,
                    QuickFix.Fields.OrderID aOrderID,
                    QuickFix.Fields.CQG_PeggedStopPx aCQG_PeggedStopPx
                ) : this()
            {
                this.Account = aAccount;
                this.OrderID = aOrderID;
                this.CQG_PeggedStopPx = aCQG_PeggedStopPx;
            }

            public QuickFix.Fields.Account Account
            { 
                get 
                {
                    QuickFix.Fields.Account val = new QuickFix.Fields.Account();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.Account val) 
            { 
                this.Account = val;
            }
            
            public QuickFix.Fields.Account Get(QuickFix.Fields.Account val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.Account val) 
            { 
                return IsSetAccount();
            }
            
            public bool IsSetAccount() 
            { 
                return IsSetField(Tags.Account);
            }
            public QuickFix.Fields.OrderID OrderID
            { 
                get 
                {
                    QuickFix.Fields.OrderID val = new QuickFix.Fields.OrderID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.OrderID val) 
            { 
                this.OrderID = val;
            }
            
            public QuickFix.Fields.OrderID Get(QuickFix.Fields.OrderID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.OrderID val) 
            { 
                return IsSetOrderID();
            }
            
            public bool IsSetOrderID() 
            { 
                return IsSetField(Tags.OrderID);
            }
            public QuickFix.Fields.CQG_AccountName CQG_AccountName
            { 
                get 
                {
                    QuickFix.Fields.CQG_AccountName val = new QuickFix.Fields.CQG_AccountName();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_AccountName val) 
            { 
                this.CQG_AccountName = val;
            }
            
            public QuickFix.Fields.CQG_AccountName Get(QuickFix.Fields.CQG_AccountName val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_AccountName val) 
            { 
                return IsSetCQG_AccountName();
            }
            
            public bool IsSetCQG_AccountName() 
            { 
                return IsSetField(Tags.CQG_AccountName);
            }
            public QuickFix.Fields.CQG_FCMAccountNumber CQG_FCMAccountNumber
            { 
                get 
                {
                    QuickFix.Fields.CQG_FCMAccountNumber val = new QuickFix.Fields.CQG_FCMAccountNumber();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_FCMAccountNumber val) 
            { 
                this.CQG_FCMAccountNumber = val;
            }
            
            public QuickFix.Fields.CQG_FCMAccountNumber Get(QuickFix.Fields.CQG_FCMAccountNumber val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_FCMAccountNumber val) 
            { 
                return IsSetCQG_FCMAccountNumber();
            }
            
            public bool IsSetCQG_FCMAccountNumber() 
            { 
                return IsSetField(Tags.CQG_FCMAccountNumber);
            }
            public QuickFix.Fields.CQG_PeggedStopPx CQG_PeggedStopPx
            { 
                get 
                {
                    QuickFix.Fields.CQG_PeggedStopPx val = new QuickFix.Fields.CQG_PeggedStopPx();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_PeggedStopPx val) 
            { 
                this.CQG_PeggedStopPx = val;
            }
            
            public QuickFix.Fields.CQG_PeggedStopPx Get(QuickFix.Fields.CQG_PeggedStopPx val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_PeggedStopPx val) 
            { 
                return IsSetCQG_PeggedStopPx();
            }
            
            public bool IsSetCQG_PeggedStopPx() 
            { 
                return IsSetField(Tags.CQG_PeggedStopPx);
            }
            public QuickFix.Fields.CQG_PeggedPrice CQG_PeggedPrice
            { 
                get 
                {
                    QuickFix.Fields.CQG_PeggedPrice val = new QuickFix.Fields.CQG_PeggedPrice();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_PeggedPrice val) 
            { 
                this.CQG_PeggedPrice = val;
            }
            
            public QuickFix.Fields.CQG_PeggedPrice Get(QuickFix.Fields.CQG_PeggedPrice val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_PeggedPrice val) 
            { 
                return IsSetCQG_PeggedPrice();
            }
            
            public bool IsSetCQG_PeggedPrice() 
            { 
                return IsSetField(Tags.CQG_PeggedPrice);
            }
        }
    }
}
