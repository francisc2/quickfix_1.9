// This is a generated file.  Don't edit it directly!

using QuickFix.Fields;
namespace QuickFix
{
    namespace FIX42 
    {
        public class NewOrderList : Message
        {
            public const string MsgType = "E";

            public NewOrderList() : base()
            {
                this.Header.SetField(new QuickFix.Fields.MsgType("E"));
            }

            public NewOrderList(
                    QuickFix.Fields.ListID aListID,
                    QuickFix.Fields.BidType aBidType,
                    QuickFix.Fields.CQG_ContingencyType aCQG_ContingencyType,
                    QuickFix.Fields.TotNoOrders aTotNoOrders,
                    QuickFix.Fields.CQG_TotNoOrderLists aCQG_TotNoOrderLists
                ) : this()
            {
                this.ListID = aListID;
                this.BidType = aBidType;
                this.CQG_ContingencyType = aCQG_ContingencyType;
                this.TotNoOrders = aTotNoOrders;
                this.CQG_TotNoOrderLists = aCQG_TotNoOrderLists;
            }

            public QuickFix.Fields.ListID ListID
            { 
                get 
                {
                    QuickFix.Fields.ListID val = new QuickFix.Fields.ListID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.ListID val) 
            { 
                this.ListID = val;
            }
            
            public QuickFix.Fields.ListID Get(QuickFix.Fields.ListID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.ListID val) 
            { 
                return IsSetListID();
            }
            
            public bool IsSetListID() 
            { 
                return IsSetField(Tags.ListID);
            }
            public QuickFix.Fields.BidType BidType
            { 
                get 
                {
                    QuickFix.Fields.BidType val = new QuickFix.Fields.BidType();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.BidType val) 
            { 
                this.BidType = val;
            }
            
            public QuickFix.Fields.BidType Get(QuickFix.Fields.BidType val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.BidType val) 
            { 
                return IsSetBidType();
            }
            
            public bool IsSetBidType() 
            { 
                return IsSetField(Tags.BidType);
            }
            public QuickFix.Fields.CQG_ContingencyType CQG_ContingencyType
            { 
                get 
                {
                    QuickFix.Fields.CQG_ContingencyType val = new QuickFix.Fields.CQG_ContingencyType();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_ContingencyType val) 
            { 
                this.CQG_ContingencyType = val;
            }
            
            public QuickFix.Fields.CQG_ContingencyType Get(QuickFix.Fields.CQG_ContingencyType val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_ContingencyType val) 
            { 
                return IsSetCQG_ContingencyType();
            }
            
            public bool IsSetCQG_ContingencyType() 
            { 
                return IsSetField(Tags.CQG_ContingencyType);
            }
            public QuickFix.Fields.TotNoOrders TotNoOrders
            { 
                get 
                {
                    QuickFix.Fields.TotNoOrders val = new QuickFix.Fields.TotNoOrders();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.TotNoOrders val) 
            { 
                this.TotNoOrders = val;
            }
            
            public QuickFix.Fields.TotNoOrders Get(QuickFix.Fields.TotNoOrders val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.TotNoOrders val) 
            { 
                return IsSetTotNoOrders();
            }
            
            public bool IsSetTotNoOrders() 
            { 
                return IsSetField(Tags.TotNoOrders);
            }
            public QuickFix.Fields.CQG_TotNoOrderLists CQG_TotNoOrderLists
            { 
                get 
                {
                    QuickFix.Fields.CQG_TotNoOrderLists val = new QuickFix.Fields.CQG_TotNoOrderLists();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_TotNoOrderLists val) 
            { 
                this.CQG_TotNoOrderLists = val;
            }
            
            public QuickFix.Fields.CQG_TotNoOrderLists Get(QuickFix.Fields.CQG_TotNoOrderLists val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_TotNoOrderLists val) 
            { 
                return IsSetCQG_TotNoOrderLists();
            }
            
            public bool IsSetCQG_TotNoOrderLists() 
            { 
                return IsSetField(Tags.CQG_TotNoOrderLists);
            }
            public QuickFix.Fields.NoOrders NoOrders
            { 
                get 
                {
                    QuickFix.Fields.NoOrders val = new QuickFix.Fields.NoOrders();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.NoOrders val) 
            { 
                this.NoOrders = val;
            }
            
            public QuickFix.Fields.NoOrders Get(QuickFix.Fields.NoOrders val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.NoOrders val) 
            { 
                return IsSetNoOrders();
            }
            
            public bool IsSetNoOrders() 
            { 
                return IsSetField(Tags.NoOrders);
            }
            public QuickFix.Fields.CQG_NoOrderLists CQG_NoOrderLists
            { 
                get 
                {
                    QuickFix.Fields.CQG_NoOrderLists val = new QuickFix.Fields.CQG_NoOrderLists();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_NoOrderLists val) 
            { 
                this.CQG_NoOrderLists = val;
            }
            
            public QuickFix.Fields.CQG_NoOrderLists Get(QuickFix.Fields.CQG_NoOrderLists val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_NoOrderLists val) 
            { 
                return IsSetCQG_NoOrderLists();
            }
            
            public bool IsSetCQG_NoOrderLists() 
            { 
                return IsSetField(Tags.CQG_NoOrderLists);
            }
            public class NoOrdersGroup : Group
            {
                public static int[] fieldOrder = {Tags.Account, Tags.ClOrdID, Tags.ListSeqNo, Tags.HandlInst, Tags.IDSource, Tags.OrderQty, Tags.OrdType, Tags.Price, Tags.SecurityID, Tags.Side, Tags.Symbol, Tags.TimeInForce, Tags.TransactTime, Tags.SymbolSfx, Tags.OpenClose, Tags.StopPx, Tags.ExDestination, Tags.MaxShow, Tags.ExpireDate, Tags.MaturityDate, Tags.CQG_ManualOrderIndicator, Tags.CQG_Aggressive, Tags.CQG_TriggerQty, 0};
            
                public NoOrdersGroup() 
                  :base( Tags.NoOrders, Tags.Account, fieldOrder)
                {
                }
            
                public override Group Clone()
                {
                    var clone = new NoOrdersGroup();
                    clone.CopyStateFrom(this);
                    return clone;
                }
            
                public QuickFix.Fields.Account Account
                { 
                    get 
                    {
                        QuickFix.Fields.Account val = new QuickFix.Fields.Account();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.Account val) 
                { 
                    this.Account = val;
                }
                
                public QuickFix.Fields.Account Get(QuickFix.Fields.Account val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.Account val) 
                { 
                    return IsSetAccount();
                }
                
                public bool IsSetAccount() 
                { 
                    return IsSetField(Tags.Account);
                }
                public QuickFix.Fields.ClOrdID ClOrdID
                { 
                    get 
                    {
                        QuickFix.Fields.ClOrdID val = new QuickFix.Fields.ClOrdID();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.ClOrdID val) 
                { 
                    this.ClOrdID = val;
                }
                
                public QuickFix.Fields.ClOrdID Get(QuickFix.Fields.ClOrdID val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.ClOrdID val) 
                { 
                    return IsSetClOrdID();
                }
                
                public bool IsSetClOrdID() 
                { 
                    return IsSetField(Tags.ClOrdID);
                }
                public QuickFix.Fields.ListSeqNo ListSeqNo
                { 
                    get 
                    {
                        QuickFix.Fields.ListSeqNo val = new QuickFix.Fields.ListSeqNo();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.ListSeqNo val) 
                { 
                    this.ListSeqNo = val;
                }
                
                public QuickFix.Fields.ListSeqNo Get(QuickFix.Fields.ListSeqNo val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.ListSeqNo val) 
                { 
                    return IsSetListSeqNo();
                }
                
                public bool IsSetListSeqNo() 
                { 
                    return IsSetField(Tags.ListSeqNo);
                }
                public QuickFix.Fields.HandlInst HandlInst
                { 
                    get 
                    {
                        QuickFix.Fields.HandlInst val = new QuickFix.Fields.HandlInst();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.HandlInst val) 
                { 
                    this.HandlInst = val;
                }
                
                public QuickFix.Fields.HandlInst Get(QuickFix.Fields.HandlInst val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.HandlInst val) 
                { 
                    return IsSetHandlInst();
                }
                
                public bool IsSetHandlInst() 
                { 
                    return IsSetField(Tags.HandlInst);
                }
                public QuickFix.Fields.IDSource IDSource
                { 
                    get 
                    {
                        QuickFix.Fields.IDSource val = new QuickFix.Fields.IDSource();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IDSource val) 
                { 
                    this.IDSource = val;
                }
                
                public QuickFix.Fields.IDSource Get(QuickFix.Fields.IDSource val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IDSource val) 
                { 
                    return IsSetIDSource();
                }
                
                public bool IsSetIDSource() 
                { 
                    return IsSetField(Tags.IDSource);
                }
                public QuickFix.Fields.OrderQty OrderQty
                { 
                    get 
                    {
                        QuickFix.Fields.OrderQty val = new QuickFix.Fields.OrderQty();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.OrderQty val) 
                { 
                    this.OrderQty = val;
                }
                
                public QuickFix.Fields.OrderQty Get(QuickFix.Fields.OrderQty val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.OrderQty val) 
                { 
                    return IsSetOrderQty();
                }
                
                public bool IsSetOrderQty() 
                { 
                    return IsSetField(Tags.OrderQty);
                }
                public QuickFix.Fields.OrdType OrdType
                { 
                    get 
                    {
                        QuickFix.Fields.OrdType val = new QuickFix.Fields.OrdType();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.OrdType val) 
                { 
                    this.OrdType = val;
                }
                
                public QuickFix.Fields.OrdType Get(QuickFix.Fields.OrdType val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.OrdType val) 
                { 
                    return IsSetOrdType();
                }
                
                public bool IsSetOrdType() 
                { 
                    return IsSetField(Tags.OrdType);
                }
                public QuickFix.Fields.Price Price
                { 
                    get 
                    {
                        QuickFix.Fields.Price val = new QuickFix.Fields.Price();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.Price val) 
                { 
                    this.Price = val;
                }
                
                public QuickFix.Fields.Price Get(QuickFix.Fields.Price val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.Price val) 
                { 
                    return IsSetPrice();
                }
                
                public bool IsSetPrice() 
                { 
                    return IsSetField(Tags.Price);
                }
                public QuickFix.Fields.SecurityID SecurityID
                { 
                    get 
                    {
                        QuickFix.Fields.SecurityID val = new QuickFix.Fields.SecurityID();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.SecurityID val) 
                { 
                    this.SecurityID = val;
                }
                
                public QuickFix.Fields.SecurityID Get(QuickFix.Fields.SecurityID val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.SecurityID val) 
                { 
                    return IsSetSecurityID();
                }
                
                public bool IsSetSecurityID() 
                { 
                    return IsSetField(Tags.SecurityID);
                }
                public QuickFix.Fields.Side Side
                { 
                    get 
                    {
                        QuickFix.Fields.Side val = new QuickFix.Fields.Side();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.Side val) 
                { 
                    this.Side = val;
                }
                
                public QuickFix.Fields.Side Get(QuickFix.Fields.Side val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.Side val) 
                { 
                    return IsSetSide();
                }
                
                public bool IsSetSide() 
                { 
                    return IsSetField(Tags.Side);
                }
                public QuickFix.Fields.Symbol Symbol
                { 
                    get 
                    {
                        QuickFix.Fields.Symbol val = new QuickFix.Fields.Symbol();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.Symbol val) 
                { 
                    this.Symbol = val;
                }
                
                public QuickFix.Fields.Symbol Get(QuickFix.Fields.Symbol val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.Symbol val) 
                { 
                    return IsSetSymbol();
                }
                
                public bool IsSetSymbol() 
                { 
                    return IsSetField(Tags.Symbol);
                }
                public QuickFix.Fields.TimeInForce TimeInForce
                { 
                    get 
                    {
                        QuickFix.Fields.TimeInForce val = new QuickFix.Fields.TimeInForce();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.TimeInForce val) 
                { 
                    this.TimeInForce = val;
                }
                
                public QuickFix.Fields.TimeInForce Get(QuickFix.Fields.TimeInForce val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.TimeInForce val) 
                { 
                    return IsSetTimeInForce();
                }
                
                public bool IsSetTimeInForce() 
                { 
                    return IsSetField(Tags.TimeInForce);
                }
                public QuickFix.Fields.TransactTime TransactTime
                { 
                    get 
                    {
                        QuickFix.Fields.TransactTime val = new QuickFix.Fields.TransactTime();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.TransactTime val) 
                { 
                    this.TransactTime = val;
                }
                
                public QuickFix.Fields.TransactTime Get(QuickFix.Fields.TransactTime val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.TransactTime val) 
                { 
                    return IsSetTransactTime();
                }
                
                public bool IsSetTransactTime() 
                { 
                    return IsSetField(Tags.TransactTime);
                }
                public QuickFix.Fields.SymbolSfx SymbolSfx
                { 
                    get 
                    {
                        QuickFix.Fields.SymbolSfx val = new QuickFix.Fields.SymbolSfx();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.SymbolSfx val) 
                { 
                    this.SymbolSfx = val;
                }
                
                public QuickFix.Fields.SymbolSfx Get(QuickFix.Fields.SymbolSfx val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.SymbolSfx val) 
                { 
                    return IsSetSymbolSfx();
                }
                
                public bool IsSetSymbolSfx() 
                { 
                    return IsSetField(Tags.SymbolSfx);
                }
                public QuickFix.Fields.OpenClose OpenClose
                { 
                    get 
                    {
                        QuickFix.Fields.OpenClose val = new QuickFix.Fields.OpenClose();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.OpenClose val) 
                { 
                    this.OpenClose = val;
                }
                
                public QuickFix.Fields.OpenClose Get(QuickFix.Fields.OpenClose val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.OpenClose val) 
                { 
                    return IsSetOpenClose();
                }
                
                public bool IsSetOpenClose() 
                { 
                    return IsSetField(Tags.OpenClose);
                }
                public QuickFix.Fields.StopPx StopPx
                { 
                    get 
                    {
                        QuickFix.Fields.StopPx val = new QuickFix.Fields.StopPx();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.StopPx val) 
                { 
                    this.StopPx = val;
                }
                
                public QuickFix.Fields.StopPx Get(QuickFix.Fields.StopPx val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.StopPx val) 
                { 
                    return IsSetStopPx();
                }
                
                public bool IsSetStopPx() 
                { 
                    return IsSetField(Tags.StopPx);
                }
                public QuickFix.Fields.ExDestination ExDestination
                { 
                    get 
                    {
                        QuickFix.Fields.ExDestination val = new QuickFix.Fields.ExDestination();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.ExDestination val) 
                { 
                    this.ExDestination = val;
                }
                
                public QuickFix.Fields.ExDestination Get(QuickFix.Fields.ExDestination val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.ExDestination val) 
                { 
                    return IsSetExDestination();
                }
                
                public bool IsSetExDestination() 
                { 
                    return IsSetField(Tags.ExDestination);
                }
                public QuickFix.Fields.MaxShow MaxShow
                { 
                    get 
                    {
                        QuickFix.Fields.MaxShow val = new QuickFix.Fields.MaxShow();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.MaxShow val) 
                { 
                    this.MaxShow = val;
                }
                
                public QuickFix.Fields.MaxShow Get(QuickFix.Fields.MaxShow val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.MaxShow val) 
                { 
                    return IsSetMaxShow();
                }
                
                public bool IsSetMaxShow() 
                { 
                    return IsSetField(Tags.MaxShow);
                }
                public QuickFix.Fields.ExpireDate ExpireDate
                { 
                    get 
                    {
                        QuickFix.Fields.ExpireDate val = new QuickFix.Fields.ExpireDate();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.ExpireDate val) 
                { 
                    this.ExpireDate = val;
                }
                
                public QuickFix.Fields.ExpireDate Get(QuickFix.Fields.ExpireDate val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.ExpireDate val) 
                { 
                    return IsSetExpireDate();
                }
                
                public bool IsSetExpireDate() 
                { 
                    return IsSetField(Tags.ExpireDate);
                }
                public QuickFix.Fields.MaturityDate MaturityDate
                { 
                    get 
                    {
                        QuickFix.Fields.MaturityDate val = new QuickFix.Fields.MaturityDate();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.MaturityDate val) 
                { 
                    this.MaturityDate = val;
                }
                
                public QuickFix.Fields.MaturityDate Get(QuickFix.Fields.MaturityDate val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.MaturityDate val) 
                { 
                    return IsSetMaturityDate();
                }
                
                public bool IsSetMaturityDate() 
                { 
                    return IsSetField(Tags.MaturityDate);
                }
                public QuickFix.Fields.CQG_ManualOrderIndicator CQG_ManualOrderIndicator
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_ManualOrderIndicator val = new QuickFix.Fields.CQG_ManualOrderIndicator();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_ManualOrderIndicator val) 
                { 
                    this.CQG_ManualOrderIndicator = val;
                }
                
                public QuickFix.Fields.CQG_ManualOrderIndicator Get(QuickFix.Fields.CQG_ManualOrderIndicator val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_ManualOrderIndicator val) 
                { 
                    return IsSetCQG_ManualOrderIndicator();
                }
                
                public bool IsSetCQG_ManualOrderIndicator() 
                { 
                    return IsSetField(Tags.CQG_ManualOrderIndicator);
                }
                public QuickFix.Fields.CQG_Aggressive CQG_Aggressive
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_Aggressive val = new QuickFix.Fields.CQG_Aggressive();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_Aggressive val) 
                { 
                    this.CQG_Aggressive = val;
                }
                
                public QuickFix.Fields.CQG_Aggressive Get(QuickFix.Fields.CQG_Aggressive val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_Aggressive val) 
                { 
                    return IsSetCQG_Aggressive();
                }
                
                public bool IsSetCQG_Aggressive() 
                { 
                    return IsSetField(Tags.CQG_Aggressive);
                }
                public QuickFix.Fields.CQG_TriggerQty CQG_TriggerQty
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_TriggerQty val = new QuickFix.Fields.CQG_TriggerQty();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_TriggerQty val) 
                { 
                    this.CQG_TriggerQty = val;
                }
                
                public QuickFix.Fields.CQG_TriggerQty Get(QuickFix.Fields.CQG_TriggerQty val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_TriggerQty val) 
                { 
                    return IsSetCQG_TriggerQty();
                }
                
                public bool IsSetCQG_TriggerQty() 
                { 
                    return IsSetField(Tags.CQG_TriggerQty);
                }
            
            }
            public class CQG_NoOrderListsGroup : Group
            {
                public static int[] fieldOrder = {Tags.CQG_NestedListSeqNo, 0};
            
                public CQG_NoOrderListsGroup() 
                  :base( Tags.CQG_NoOrderLists, Tags.CQG_NestedListSeqNo, fieldOrder)
                {
                }
            
                public override Group Clone()
                {
                    var clone = new CQG_NoOrderListsGroup();
                    clone.CopyStateFrom(this);
                    return clone;
                }
            
                public QuickFix.Fields.CQG_NestedListSeqNo CQG_NestedListSeqNo
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_NestedListSeqNo val = new QuickFix.Fields.CQG_NestedListSeqNo();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_NestedListSeqNo val) 
                { 
                    this.CQG_NestedListSeqNo = val;
                }
                
                public QuickFix.Fields.CQG_NestedListSeqNo Get(QuickFix.Fields.CQG_NestedListSeqNo val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_NestedListSeqNo val) 
                { 
                    return IsSetCQG_NestedListSeqNo();
                }
                
                public bool IsSetCQG_NestedListSeqNo() 
                { 
                    return IsSetField(Tags.CQG_NestedListSeqNo);
                }
            
            }
        }
    }
}
