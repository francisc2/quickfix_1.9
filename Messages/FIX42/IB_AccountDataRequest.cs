// This is a generated file.  Don't edit it directly!

using QuickFix.Fields;
namespace QuickFix
{
    namespace FIX42 
    {
        public class IB_AccountDataRequest : Message
        {
            public const string MsgType = "U";

            public IB_AccountDataRequest() : base()
            {
                this.Header.SetField(new QuickFix.Fields.MsgType("U"));
            }


            public QuickFix.Fields.IB_ReqID IB_ReqID
            { 
                get 
                {
                    QuickFix.Fields.IB_ReqID val = new QuickFix.Fields.IB_ReqID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.IB_ReqID val) 
            { 
                this.IB_ReqID = val;
            }
            
            public QuickFix.Fields.IB_ReqID Get(QuickFix.Fields.IB_ReqID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.IB_ReqID val) 
            { 
                return IsSetIB_ReqID();
            }
            
            public bool IsSetIB_ReqID() 
            { 
                return IsSetField(Tags.IB_ReqID);
            }
            public QuickFix.Fields.IB_AccountReqType IB_AccountReqType
            { 
                get 
                {
                    QuickFix.Fields.IB_AccountReqType val = new QuickFix.Fields.IB_AccountReqType();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.IB_AccountReqType val) 
            { 
                this.IB_AccountReqType = val;
            }
            
            public QuickFix.Fields.IB_AccountReqType Get(QuickFix.Fields.IB_AccountReqType val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.IB_AccountReqType val) 
            { 
                return IsSetIB_AccountReqType();
            }
            
            public bool IsSetIB_AccountReqType() 
            { 
                return IsSetField(Tags.IB_AccountReqType);
            }
            public QuickFix.Fields.IB_SubMsgType IB_SubMsgType
            { 
                get 
                {
                    QuickFix.Fields.IB_SubMsgType val = new QuickFix.Fields.IB_SubMsgType();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.IB_SubMsgType val) 
            { 
                this.IB_SubMsgType = val;
            }
            
            public QuickFix.Fields.IB_SubMsgType Get(QuickFix.Fields.IB_SubMsgType val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.IB_SubMsgType val) 
            { 
                return IsSetIB_SubMsgType();
            }
            
            public bool IsSetIB_SubMsgType() 
            { 
                return IsSetField(Tags.IB_SubMsgType);
            }
            public QuickFix.Fields.IB_AccountCode IB_AccountCode
            { 
                get 
                {
                    QuickFix.Fields.IB_AccountCode val = new QuickFix.Fields.IB_AccountCode();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.IB_AccountCode val) 
            { 
                this.IB_AccountCode = val;
            }
            
            public QuickFix.Fields.IB_AccountCode Get(QuickFix.Fields.IB_AccountCode val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.IB_AccountCode val) 
            { 
                return IsSetIB_AccountCode();
            }
            
            public bool IsSetIB_AccountCode() 
            { 
                return IsSetField(Tags.IB_AccountCode);
            }
            public QuickFix.Fields.IB_Commission IB_Commission
            { 
                get 
                {
                    QuickFix.Fields.IB_Commission val = new QuickFix.Fields.IB_Commission();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.IB_Commission val) 
            { 
                this.IB_Commission = val;
            }
            
            public QuickFix.Fields.IB_Commission Get(QuickFix.Fields.IB_Commission val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.IB_Commission val) 
            { 
                return IsSetIB_Commission();
            }
            
            public bool IsSetIB_Commission() 
            { 
                return IsSetField(Tags.IB_Commission);
            }
            public QuickFix.Fields.IB_Currency IB_Currency
            { 
                get 
                {
                    QuickFix.Fields.IB_Currency val = new QuickFix.Fields.IB_Currency();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.IB_Currency val) 
            { 
                this.IB_Currency = val;
            }
            
            public QuickFix.Fields.IB_Currency Get(QuickFix.Fields.IB_Currency val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.IB_Currency val) 
            { 
                return IsSetIB_Currency();
            }
            
            public bool IsSetIB_Currency() 
            { 
                return IsSetField(Tags.IB_Currency);
            }
            public QuickFix.Fields.Text Text
            { 
                get 
                {
                    QuickFix.Fields.Text val = new QuickFix.Fields.Text();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.Text val) 
            { 
                this.Text = val;
            }
            
            public QuickFix.Fields.Text Get(QuickFix.Fields.Text val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.Text val) 
            { 
                return IsSetText();
            }
            
            public bool IsSetText() 
            { 
                return IsSetField(Tags.Text);
            }
            public QuickFix.Fields.OrderID OrderID
            { 
                get 
                {
                    QuickFix.Fields.OrderID val = new QuickFix.Fields.OrderID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.OrderID val) 
            { 
                this.OrderID = val;
            }
            
            public QuickFix.Fields.OrderID Get(QuickFix.Fields.OrderID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.OrderID val) 
            { 
                return IsSetOrderID();
            }
            
            public bool IsSetOrderID() 
            { 
                return IsSetField(Tags.OrderID);
            }
            public QuickFix.Fields.ExecID ExecID
            { 
                get 
                {
                    QuickFix.Fields.ExecID val = new QuickFix.Fields.ExecID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.ExecID val) 
            { 
                this.ExecID = val;
            }
            
            public QuickFix.Fields.ExecID Get(QuickFix.Fields.ExecID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.ExecID val) 
            { 
                return IsSetExecID();
            }
            
            public bool IsSetExecID() 
            { 
                return IsSetField(Tags.ExecID);
            }
            public QuickFix.Fields.IB_RealizedPnL IB_RealizedPnL
            { 
                get 
                {
                    QuickFix.Fields.IB_RealizedPnL val = new QuickFix.Fields.IB_RealizedPnL();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.IB_RealizedPnL val) 
            { 
                this.IB_RealizedPnL = val;
            }
            
            public QuickFix.Fields.IB_RealizedPnL Get(QuickFix.Fields.IB_RealizedPnL val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.IB_RealizedPnL val) 
            { 
                return IsSetIB_RealizedPnL();
            }
            
            public bool IsSetIB_RealizedPnL() 
            { 
                return IsSetField(Tags.IB_RealizedPnL);
            }
        }
    }
}
