// This is a generated file.  Don't edit it directly!

using QuickFix.Fields;
namespace QuickFix
{
    namespace FIX42 
    {
        public class CQG_AccountDataRequest : Message
        {
            public const string MsgType = "UAR";

            public CQG_AccountDataRequest() : base()
            {
                this.Header.SetField(new QuickFix.Fields.MsgType("UAR"));
            }

            public CQG_AccountDataRequest(
                    QuickFix.Fields.CQG_AcctReqID aCQG_AcctReqID
                ) : this()
            {
                this.CQG_AcctReqID = aCQG_AcctReqID;
            }

            public QuickFix.Fields.Account Account
            { 
                get 
                {
                    QuickFix.Fields.Account val = new QuickFix.Fields.Account();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.Account val) 
            { 
                this.Account = val;
            }
            
            public QuickFix.Fields.Account Get(QuickFix.Fields.Account val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.Account val) 
            { 
                return IsSetAccount();
            }
            
            public bool IsSetAccount() 
            { 
                return IsSetField(Tags.Account);
            }
            public QuickFix.Fields.SubscriptionRequestType SubscriptionRequestType
            { 
                get 
                {
                    QuickFix.Fields.SubscriptionRequestType val = new QuickFix.Fields.SubscriptionRequestType();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.SubscriptionRequestType val) 
            { 
                this.SubscriptionRequestType = val;
            }
            
            public QuickFix.Fields.SubscriptionRequestType Get(QuickFix.Fields.SubscriptionRequestType val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.SubscriptionRequestType val) 
            { 
                return IsSetSubscriptionRequestType();
            }
            
            public bool IsSetSubscriptionRequestType() 
            { 
                return IsSetField(Tags.SubscriptionRequestType);
            }
            public QuickFix.Fields.CQG_AcctReqID CQG_AcctReqID
            { 
                get 
                {
                    QuickFix.Fields.CQG_AcctReqID val = new QuickFix.Fields.CQG_AcctReqID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_AcctReqID val) 
            { 
                this.CQG_AcctReqID = val;
            }
            
            public QuickFix.Fields.CQG_AcctReqID Get(QuickFix.Fields.CQG_AcctReqID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_AcctReqID val) 
            { 
                return IsSetCQG_AcctReqID();
            }
            
            public bool IsSetCQG_AcctReqID() 
            { 
                return IsSetField(Tags.CQG_AcctReqID);
            }
            public QuickFix.Fields.CQG_IncludeBalanceGrp CQG_IncludeBalanceGrp
            { 
                get 
                {
                    QuickFix.Fields.CQG_IncludeBalanceGrp val = new QuickFix.Fields.CQG_IncludeBalanceGrp();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_IncludeBalanceGrp val) 
            { 
                this.CQG_IncludeBalanceGrp = val;
            }
            
            public QuickFix.Fields.CQG_IncludeBalanceGrp Get(QuickFix.Fields.CQG_IncludeBalanceGrp val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_IncludeBalanceGrp val) 
            { 
                return IsSetCQG_IncludeBalanceGrp();
            }
            
            public bool IsSetCQG_IncludeBalanceGrp() 
            { 
                return IsSetField(Tags.CQG_IncludeBalanceGrp);
            }
        }
    }
}
