// This is a generated file.  Don't edit it directly!

using QuickFix.Fields;
namespace QuickFix
{
    namespace FIX42 
    {
        public class CQG_AllowedFeatureReport : Message
        {
            public const string MsgType = "UZW";

            public CQG_AllowedFeatureReport() : base()
            {
                this.Header.SetField(new QuickFix.Fields.MsgType("UZW"));
            }

            public CQG_AllowedFeatureReport(
                    QuickFix.Fields.CQG_RequestID aCQG_RequestID,
                    QuickFix.Fields.CQG_LastFragment aCQG_LastFragment
                ) : this()
            {
                this.CQG_RequestID = aCQG_RequestID;
                this.CQG_LastFragment = aCQG_LastFragment;
            }

            public QuickFix.Fields.CQG_RequestID CQG_RequestID
            { 
                get 
                {
                    QuickFix.Fields.CQG_RequestID val = new QuickFix.Fields.CQG_RequestID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_RequestID val) 
            { 
                this.CQG_RequestID = val;
            }
            
            public QuickFix.Fields.CQG_RequestID Get(QuickFix.Fields.CQG_RequestID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_RequestID val) 
            { 
                return IsSetCQG_RequestID();
            }
            
            public bool IsSetCQG_RequestID() 
            { 
                return IsSetField(Tags.CQG_RequestID);
            }
            public QuickFix.Fields.CQG_LastFragment CQG_LastFragment
            { 
                get 
                {
                    QuickFix.Fields.CQG_LastFragment val = new QuickFix.Fields.CQG_LastFragment();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_LastFragment val) 
            { 
                this.CQG_LastFragment = val;
            }
            
            public QuickFix.Fields.CQG_LastFragment Get(QuickFix.Fields.CQG_LastFragment val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_LastFragment val) 
            { 
                return IsSetCQG_LastFragment();
            }
            
            public bool IsSetCQG_LastFragment() 
            { 
                return IsSetField(Tags.CQG_LastFragment);
            }
            public QuickFix.Fields.CQG_NoEnablements CQG_NoEnablements
            { 
                get 
                {
                    QuickFix.Fields.CQG_NoEnablements val = new QuickFix.Fields.CQG_NoEnablements();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_NoEnablements val) 
            { 
                this.CQG_NoEnablements = val;
            }
            
            public QuickFix.Fields.CQG_NoEnablements Get(QuickFix.Fields.CQG_NoEnablements val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_NoEnablements val) 
            { 
                return IsSetCQG_NoEnablements();
            }
            
            public bool IsSetCQG_NoEnablements() 
            { 
                return IsSetField(Tags.CQG_NoEnablements);
            }
            public class CQG_NoEnablementsGroup : Group
            {
                public static int[] fieldOrder = {Tags.CQG_EnablementID, Tags.CQG_EnablementGroup, Tags.CQG_BillingGroupID, Tags.CQG_BillingID, 0};
            
                public CQG_NoEnablementsGroup() 
                  :base( Tags.CQG_NoEnablements, Tags.CQG_EnablementID, fieldOrder)
                {
                }
            
                public override Group Clone()
                {
                    var clone = new CQG_NoEnablementsGroup();
                    clone.CopyStateFrom(this);
                    return clone;
                }
            
                public QuickFix.Fields.CQG_EnablementID CQG_EnablementID
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_EnablementID val = new QuickFix.Fields.CQG_EnablementID();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_EnablementID val) 
                { 
                    this.CQG_EnablementID = val;
                }
                
                public QuickFix.Fields.CQG_EnablementID Get(QuickFix.Fields.CQG_EnablementID val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_EnablementID val) 
                { 
                    return IsSetCQG_EnablementID();
                }
                
                public bool IsSetCQG_EnablementID() 
                { 
                    return IsSetField(Tags.CQG_EnablementID);
                }
                public QuickFix.Fields.CQG_EnablementGroup CQG_EnablementGroup
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_EnablementGroup val = new QuickFix.Fields.CQG_EnablementGroup();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_EnablementGroup val) 
                { 
                    this.CQG_EnablementGroup = val;
                }
                
                public QuickFix.Fields.CQG_EnablementGroup Get(QuickFix.Fields.CQG_EnablementGroup val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_EnablementGroup val) 
                { 
                    return IsSetCQG_EnablementGroup();
                }
                
                public bool IsSetCQG_EnablementGroup() 
                { 
                    return IsSetField(Tags.CQG_EnablementGroup);
                }
                public QuickFix.Fields.CQG_BillingGroupID CQG_BillingGroupID
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_BillingGroupID val = new QuickFix.Fields.CQG_BillingGroupID();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_BillingGroupID val) 
                { 
                    this.CQG_BillingGroupID = val;
                }
                
                public QuickFix.Fields.CQG_BillingGroupID Get(QuickFix.Fields.CQG_BillingGroupID val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_BillingGroupID val) 
                { 
                    return IsSetCQG_BillingGroupID();
                }
                
                public bool IsSetCQG_BillingGroupID() 
                { 
                    return IsSetField(Tags.CQG_BillingGroupID);
                }
                public QuickFix.Fields.CQG_BillingID CQG_BillingID
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_BillingID val = new QuickFix.Fields.CQG_BillingID();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_BillingID val) 
                { 
                    this.CQG_BillingID = val;
                }
                
                public QuickFix.Fields.CQG_BillingID Get(QuickFix.Fields.CQG_BillingID val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_BillingID val) 
                { 
                    return IsSetCQG_BillingID();
                }
                
                public bool IsSetCQG_BillingID() 
                { 
                    return IsSetField(Tags.CQG_BillingID);
                }
            
            }
        }
    }
}
