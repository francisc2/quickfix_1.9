// This is a generated file.  Don't edit it directly!

using QuickFix.Fields;
namespace QuickFix
{
    namespace FIX42 
    {
        public class CQG_CollateralReport : Message
        {
            public const string MsgType = "UBA";

            public CQG_CollateralReport() : base()
            {
                this.Header.SetField(new QuickFix.Fields.MsgType("UBA"));
            }

            public CQG_CollateralReport(
                    QuickFix.Fields.CQG_CollInquiryID aCQG_CollInquiryID,
                    QuickFix.Fields.Account aAccount,
                    QuickFix.Fields.CQG_TotalMarginReqs aCQG_TotalMarginReqs,
                    QuickFix.Fields.CQG_PurchasingPower aCQG_PurchasingPower,
                    QuickFix.Fields.Currency aCurrency
                ) : this()
            {
                this.CQG_CollInquiryID = aCQG_CollInquiryID;
                this.Account = aAccount;
                this.CQG_TotalMarginReqs = aCQG_TotalMarginReqs;
                this.CQG_PurchasingPower = aCQG_PurchasingPower;
                this.Currency = aCurrency;
            }

            public QuickFix.Fields.CQG_CollInquiryID CQG_CollInquiryID
            { 
                get 
                {
                    QuickFix.Fields.CQG_CollInquiryID val = new QuickFix.Fields.CQG_CollInquiryID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_CollInquiryID val) 
            { 
                this.CQG_CollInquiryID = val;
            }
            
            public QuickFix.Fields.CQG_CollInquiryID Get(QuickFix.Fields.CQG_CollInquiryID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_CollInquiryID val) 
            { 
                return IsSetCQG_CollInquiryID();
            }
            
            public bool IsSetCQG_CollInquiryID() 
            { 
                return IsSetField(Tags.CQG_CollInquiryID);
            }
            public QuickFix.Fields.Account Account
            { 
                get 
                {
                    QuickFix.Fields.Account val = new QuickFix.Fields.Account();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.Account val) 
            { 
                this.Account = val;
            }
            
            public QuickFix.Fields.Account Get(QuickFix.Fields.Account val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.Account val) 
            { 
                return IsSetAccount();
            }
            
            public bool IsSetAccount() 
            { 
                return IsSetField(Tags.Account);
            }
            public QuickFix.Fields.CQG_TotalMarginReqs CQG_TotalMarginReqs
            { 
                get 
                {
                    QuickFix.Fields.CQG_TotalMarginReqs val = new QuickFix.Fields.CQG_TotalMarginReqs();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_TotalMarginReqs val) 
            { 
                this.CQG_TotalMarginReqs = val;
            }
            
            public QuickFix.Fields.CQG_TotalMarginReqs Get(QuickFix.Fields.CQG_TotalMarginReqs val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_TotalMarginReqs val) 
            { 
                return IsSetCQG_TotalMarginReqs();
            }
            
            public bool IsSetCQG_TotalMarginReqs() 
            { 
                return IsSetField(Tags.CQG_TotalMarginReqs);
            }
            public QuickFix.Fields.CQG_PurchasingPower CQG_PurchasingPower
            { 
                get 
                {
                    QuickFix.Fields.CQG_PurchasingPower val = new QuickFix.Fields.CQG_PurchasingPower();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_PurchasingPower val) 
            { 
                this.CQG_PurchasingPower = val;
            }
            
            public QuickFix.Fields.CQG_PurchasingPower Get(QuickFix.Fields.CQG_PurchasingPower val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_PurchasingPower val) 
            { 
                return IsSetCQG_PurchasingPower();
            }
            
            public bool IsSetCQG_PurchasingPower() 
            { 
                return IsSetField(Tags.CQG_PurchasingPower);
            }
            public QuickFix.Fields.Currency Currency
            { 
                get 
                {
                    QuickFix.Fields.Currency val = new QuickFix.Fields.Currency();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.Currency val) 
            { 
                this.Currency = val;
            }
            
            public QuickFix.Fields.Currency Get(QuickFix.Fields.Currency val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.Currency val) 
            { 
                return IsSetCurrency();
            }
            
            public bool IsSetCurrency() 
            { 
                return IsSetField(Tags.Currency);
            }
            public QuickFix.Fields.CQG_LastRptRequested CQG_LastRptRequested
            { 
                get 
                {
                    QuickFix.Fields.CQG_LastRptRequested val = new QuickFix.Fields.CQG_LastRptRequested();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_LastRptRequested val) 
            { 
                this.CQG_LastRptRequested = val;
            }
            
            public QuickFix.Fields.CQG_LastRptRequested Get(QuickFix.Fields.CQG_LastRptRequested val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_LastRptRequested val) 
            { 
                return IsSetCQG_LastRptRequested();
            }
            
            public bool IsSetCQG_LastRptRequested() 
            { 
                return IsSetField(Tags.CQG_LastRptRequested);
            }
        }
    }
}
