// This is a generated file.  Don't edit it directly!

using QuickFix.Fields;
namespace QuickFix
{
    namespace FIX42 
    {
        public class CQG_AuthorizedAccountsRequest : Message
        {
            public const string MsgType = "UZS";

            public CQG_AuthorizedAccountsRequest() : base()
            {
                this.Header.SetField(new QuickFix.Fields.MsgType("UZS"));
            }

            public CQG_AuthorizedAccountsRequest(
                    QuickFix.Fields.CQG_RequestID aCQG_RequestID
                ) : this()
            {
                this.CQG_RequestID = aCQG_RequestID;
            }

            public QuickFix.Fields.CQG_RequestID CQG_RequestID
            { 
                get 
                {
                    QuickFix.Fields.CQG_RequestID val = new QuickFix.Fields.CQG_RequestID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_RequestID val) 
            { 
                this.CQG_RequestID = val;
            }
            
            public QuickFix.Fields.CQG_RequestID Get(QuickFix.Fields.CQG_RequestID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_RequestID val) 
            { 
                return IsSetCQG_RequestID();
            }
            
            public bool IsSetCQG_RequestID() 
            { 
                return IsSetField(Tags.CQG_RequestID);
            }
            public QuickFix.Fields.SubscriptionRequestType SubscriptionRequestType
            { 
                get 
                {
                    QuickFix.Fields.SubscriptionRequestType val = new QuickFix.Fields.SubscriptionRequestType();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.SubscriptionRequestType val) 
            { 
                this.SubscriptionRequestType = val;
            }
            
            public QuickFix.Fields.SubscriptionRequestType Get(QuickFix.Fields.SubscriptionRequestType val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.SubscriptionRequestType val) 
            { 
                return IsSetSubscriptionRequestType();
            }
            
            public bool IsSetSubscriptionRequestType() 
            { 
                return IsSetField(Tags.SubscriptionRequestType);
            }
        }
    }
}
