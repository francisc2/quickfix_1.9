// This is a generated file.  Don't edit it directly!

using QuickFix.Fields;
namespace QuickFix
{
    namespace FIX42 
    {
        public class IB_PortfolioUpdate : Message
        {
            public const string MsgType = "UP";

            public IB_PortfolioUpdate() : base()
            {
                this.Header.SetField(new QuickFix.Fields.MsgType("UP"));
            }


            public QuickFix.Fields.IB_ReqID IB_ReqID
            { 
                get 
                {
                    QuickFix.Fields.IB_ReqID val = new QuickFix.Fields.IB_ReqID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.IB_ReqID val) 
            { 
                this.IB_ReqID = val;
            }
            
            public QuickFix.Fields.IB_ReqID Get(QuickFix.Fields.IB_ReqID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.IB_ReqID val) 
            { 
                return IsSetIB_ReqID();
            }
            
            public bool IsSetIB_ReqID() 
            { 
                return IsSetField(Tags.IB_ReqID);
            }
            public QuickFix.Fields.IB_AccountReqType IB_AccountReqType
            { 
                get 
                {
                    QuickFix.Fields.IB_AccountReqType val = new QuickFix.Fields.IB_AccountReqType();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.IB_AccountReqType val) 
            { 
                this.IB_AccountReqType = val;
            }
            
            public QuickFix.Fields.IB_AccountReqType Get(QuickFix.Fields.IB_AccountReqType val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.IB_AccountReqType val) 
            { 
                return IsSetIB_AccountReqType();
            }
            
            public bool IsSetIB_AccountReqType() 
            { 
                return IsSetField(Tags.IB_AccountReqType);
            }
            public QuickFix.Fields.IB_SubMsgType IB_SubMsgType
            { 
                get 
                {
                    QuickFix.Fields.IB_SubMsgType val = new QuickFix.Fields.IB_SubMsgType();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.IB_SubMsgType val) 
            { 
                this.IB_SubMsgType = val;
            }
            
            public QuickFix.Fields.IB_SubMsgType Get(QuickFix.Fields.IB_SubMsgType val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.IB_SubMsgType val) 
            { 
                return IsSetIB_SubMsgType();
            }
            
            public bool IsSetIB_SubMsgType() 
            { 
                return IsSetField(Tags.IB_SubMsgType);
            }
            public QuickFix.Fields.IB_AccountCode IB_AccountCode
            { 
                get 
                {
                    QuickFix.Fields.IB_AccountCode val = new QuickFix.Fields.IB_AccountCode();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.IB_AccountCode val) 
            { 
                this.IB_AccountCode = val;
            }
            
            public QuickFix.Fields.IB_AccountCode Get(QuickFix.Fields.IB_AccountCode val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.IB_AccountCode val) 
            { 
                return IsSetIB_AccountCode();
            }
            
            public bool IsSetIB_AccountCode() 
            { 
                return IsSetField(Tags.IB_AccountCode);
            }
            public QuickFix.Fields.IB_Description IB_Description
            { 
                get 
                {
                    QuickFix.Fields.IB_Description val = new QuickFix.Fields.IB_Description();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.IB_Description val) 
            { 
                this.IB_Description = val;
            }
            
            public QuickFix.Fields.IB_Description Get(QuickFix.Fields.IB_Description val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.IB_Description val) 
            { 
                return IsSetIB_Description();
            }
            
            public bool IsSetIB_Description() 
            { 
                return IsSetField(Tags.IB_Description);
            }
            public class IB_DescriptionGroup : Group
            {
                public static int[] fieldOrder = {Tags.IB_EventSeverity, Tags.IB_FirstKey, Tags.IB_Value, Tags.IB_Position, Tags.IB_MarketValue, Tags.IB_MarketPrice, Tags.IB_TimeStamp, Tags.Currency, Tags.IB_ContractID, Tags.SecurityType, Tags.IB_AvgCost, Tags.IB_AvgPx, Tags.IB_DLVPerContract, Tags.IB_RealizedPnL, Tags.IB_UnrealizedPnL, Tags.IB_LastLiquidate, Tags.IB_SLBAvailableForLoan, Tags.IB_SLBPrivateLocate, 0};
            
                public IB_DescriptionGroup() 
                  :base( Tags.IB_Description, Tags.IB_EventSeverity, fieldOrder)
                {
                }
            
                public override Group Clone()
                {
                    var clone = new IB_DescriptionGroup();
                    clone.CopyStateFrom(this);
                    return clone;
                }
            
                public QuickFix.Fields.IB_EventSeverity IB_EventSeverity
                { 
                    get 
                    {
                        QuickFix.Fields.IB_EventSeverity val = new QuickFix.Fields.IB_EventSeverity();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_EventSeverity val) 
                { 
                    this.IB_EventSeverity = val;
                }
                
                public QuickFix.Fields.IB_EventSeverity Get(QuickFix.Fields.IB_EventSeverity val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_EventSeverity val) 
                { 
                    return IsSetIB_EventSeverity();
                }
                
                public bool IsSetIB_EventSeverity() 
                { 
                    return IsSetField(Tags.IB_EventSeverity);
                }
                public QuickFix.Fields.IB_FirstKey IB_FirstKey
                { 
                    get 
                    {
                        QuickFix.Fields.IB_FirstKey val = new QuickFix.Fields.IB_FirstKey();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_FirstKey val) 
                { 
                    this.IB_FirstKey = val;
                }
                
                public QuickFix.Fields.IB_FirstKey Get(QuickFix.Fields.IB_FirstKey val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_FirstKey val) 
                { 
                    return IsSetIB_FirstKey();
                }
                
                public bool IsSetIB_FirstKey() 
                { 
                    return IsSetField(Tags.IB_FirstKey);
                }
                public QuickFix.Fields.IB_Value IB_Value
                { 
                    get 
                    {
                        QuickFix.Fields.IB_Value val = new QuickFix.Fields.IB_Value();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_Value val) 
                { 
                    this.IB_Value = val;
                }
                
                public QuickFix.Fields.IB_Value Get(QuickFix.Fields.IB_Value val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_Value val) 
                { 
                    return IsSetIB_Value();
                }
                
                public bool IsSetIB_Value() 
                { 
                    return IsSetField(Tags.IB_Value);
                }
                public QuickFix.Fields.IB_Position IB_Position
                { 
                    get 
                    {
                        QuickFix.Fields.IB_Position val = new QuickFix.Fields.IB_Position();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_Position val) 
                { 
                    this.IB_Position = val;
                }
                
                public QuickFix.Fields.IB_Position Get(QuickFix.Fields.IB_Position val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_Position val) 
                { 
                    return IsSetIB_Position();
                }
                
                public bool IsSetIB_Position() 
                { 
                    return IsSetField(Tags.IB_Position);
                }
                public QuickFix.Fields.IB_MarketValue IB_MarketValue
                { 
                    get 
                    {
                        QuickFix.Fields.IB_MarketValue val = new QuickFix.Fields.IB_MarketValue();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_MarketValue val) 
                { 
                    this.IB_MarketValue = val;
                }
                
                public QuickFix.Fields.IB_MarketValue Get(QuickFix.Fields.IB_MarketValue val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_MarketValue val) 
                { 
                    return IsSetIB_MarketValue();
                }
                
                public bool IsSetIB_MarketValue() 
                { 
                    return IsSetField(Tags.IB_MarketValue);
                }
                public QuickFix.Fields.IB_MarketPrice IB_MarketPrice
                { 
                    get 
                    {
                        QuickFix.Fields.IB_MarketPrice val = new QuickFix.Fields.IB_MarketPrice();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_MarketPrice val) 
                { 
                    this.IB_MarketPrice = val;
                }
                
                public QuickFix.Fields.IB_MarketPrice Get(QuickFix.Fields.IB_MarketPrice val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_MarketPrice val) 
                { 
                    return IsSetIB_MarketPrice();
                }
                
                public bool IsSetIB_MarketPrice() 
                { 
                    return IsSetField(Tags.IB_MarketPrice);
                }
                public QuickFix.Fields.IB_TimeStamp IB_TimeStamp
                { 
                    get 
                    {
                        QuickFix.Fields.IB_TimeStamp val = new QuickFix.Fields.IB_TimeStamp();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_TimeStamp val) 
                { 
                    this.IB_TimeStamp = val;
                }
                
                public QuickFix.Fields.IB_TimeStamp Get(QuickFix.Fields.IB_TimeStamp val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_TimeStamp val) 
                { 
                    return IsSetIB_TimeStamp();
                }
                
                public bool IsSetIB_TimeStamp() 
                { 
                    return IsSetField(Tags.IB_TimeStamp);
                }
                public QuickFix.Fields.Currency Currency
                { 
                    get 
                    {
                        QuickFix.Fields.Currency val = new QuickFix.Fields.Currency();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.Currency val) 
                { 
                    this.Currency = val;
                }
                
                public QuickFix.Fields.Currency Get(QuickFix.Fields.Currency val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.Currency val) 
                { 
                    return IsSetCurrency();
                }
                
                public bool IsSetCurrency() 
                { 
                    return IsSetField(Tags.Currency);
                }
                public QuickFix.Fields.IB_ContractID IB_ContractID
                { 
                    get 
                    {
                        QuickFix.Fields.IB_ContractID val = new QuickFix.Fields.IB_ContractID();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_ContractID val) 
                { 
                    this.IB_ContractID = val;
                }
                
                public QuickFix.Fields.IB_ContractID Get(QuickFix.Fields.IB_ContractID val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_ContractID val) 
                { 
                    return IsSetIB_ContractID();
                }
                
                public bool IsSetIB_ContractID() 
                { 
                    return IsSetField(Tags.IB_ContractID);
                }
                public QuickFix.Fields.SecurityType SecurityType
                { 
                    get 
                    {
                        QuickFix.Fields.SecurityType val = new QuickFix.Fields.SecurityType();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.SecurityType val) 
                { 
                    this.SecurityType = val;
                }
                
                public QuickFix.Fields.SecurityType Get(QuickFix.Fields.SecurityType val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.SecurityType val) 
                { 
                    return IsSetSecurityType();
                }
                
                public bool IsSetSecurityType() 
                { 
                    return IsSetField(Tags.SecurityType);
                }
                public QuickFix.Fields.IB_AvgCost IB_AvgCost
                { 
                    get 
                    {
                        QuickFix.Fields.IB_AvgCost val = new QuickFix.Fields.IB_AvgCost();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_AvgCost val) 
                { 
                    this.IB_AvgCost = val;
                }
                
                public QuickFix.Fields.IB_AvgCost Get(QuickFix.Fields.IB_AvgCost val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_AvgCost val) 
                { 
                    return IsSetIB_AvgCost();
                }
                
                public bool IsSetIB_AvgCost() 
                { 
                    return IsSetField(Tags.IB_AvgCost);
                }
                public QuickFix.Fields.IB_AvgPx IB_AvgPx
                { 
                    get 
                    {
                        QuickFix.Fields.IB_AvgPx val = new QuickFix.Fields.IB_AvgPx();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_AvgPx val) 
                { 
                    this.IB_AvgPx = val;
                }
                
                public QuickFix.Fields.IB_AvgPx Get(QuickFix.Fields.IB_AvgPx val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_AvgPx val) 
                { 
                    return IsSetIB_AvgPx();
                }
                
                public bool IsSetIB_AvgPx() 
                { 
                    return IsSetField(Tags.IB_AvgPx);
                }
                public QuickFix.Fields.IB_DLVPerContract IB_DLVPerContract
                { 
                    get 
                    {
                        QuickFix.Fields.IB_DLVPerContract val = new QuickFix.Fields.IB_DLVPerContract();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_DLVPerContract val) 
                { 
                    this.IB_DLVPerContract = val;
                }
                
                public QuickFix.Fields.IB_DLVPerContract Get(QuickFix.Fields.IB_DLVPerContract val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_DLVPerContract val) 
                { 
                    return IsSetIB_DLVPerContract();
                }
                
                public bool IsSetIB_DLVPerContract() 
                { 
                    return IsSetField(Tags.IB_DLVPerContract);
                }
                public QuickFix.Fields.IB_RealizedPnL IB_RealizedPnL
                { 
                    get 
                    {
                        QuickFix.Fields.IB_RealizedPnL val = new QuickFix.Fields.IB_RealizedPnL();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_RealizedPnL val) 
                { 
                    this.IB_RealizedPnL = val;
                }
                
                public QuickFix.Fields.IB_RealizedPnL Get(QuickFix.Fields.IB_RealizedPnL val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_RealizedPnL val) 
                { 
                    return IsSetIB_RealizedPnL();
                }
                
                public bool IsSetIB_RealizedPnL() 
                { 
                    return IsSetField(Tags.IB_RealizedPnL);
                }
                public QuickFix.Fields.IB_UnrealizedPnL IB_UnrealizedPnL
                { 
                    get 
                    {
                        QuickFix.Fields.IB_UnrealizedPnL val = new QuickFix.Fields.IB_UnrealizedPnL();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_UnrealizedPnL val) 
                { 
                    this.IB_UnrealizedPnL = val;
                }
                
                public QuickFix.Fields.IB_UnrealizedPnL Get(QuickFix.Fields.IB_UnrealizedPnL val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_UnrealizedPnL val) 
                { 
                    return IsSetIB_UnrealizedPnL();
                }
                
                public bool IsSetIB_UnrealizedPnL() 
                { 
                    return IsSetField(Tags.IB_UnrealizedPnL);
                }
                public QuickFix.Fields.IB_LastLiquidate IB_LastLiquidate
                { 
                    get 
                    {
                        QuickFix.Fields.IB_LastLiquidate val = new QuickFix.Fields.IB_LastLiquidate();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_LastLiquidate val) 
                { 
                    this.IB_LastLiquidate = val;
                }
                
                public QuickFix.Fields.IB_LastLiquidate Get(QuickFix.Fields.IB_LastLiquidate val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_LastLiquidate val) 
                { 
                    return IsSetIB_LastLiquidate();
                }
                
                public bool IsSetIB_LastLiquidate() 
                { 
                    return IsSetField(Tags.IB_LastLiquidate);
                }
                public QuickFix.Fields.IB_SLBAvailableForLoan IB_SLBAvailableForLoan
                { 
                    get 
                    {
                        QuickFix.Fields.IB_SLBAvailableForLoan val = new QuickFix.Fields.IB_SLBAvailableForLoan();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_SLBAvailableForLoan val) 
                { 
                    this.IB_SLBAvailableForLoan = val;
                }
                
                public QuickFix.Fields.IB_SLBAvailableForLoan Get(QuickFix.Fields.IB_SLBAvailableForLoan val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_SLBAvailableForLoan val) 
                { 
                    return IsSetIB_SLBAvailableForLoan();
                }
                
                public bool IsSetIB_SLBAvailableForLoan() 
                { 
                    return IsSetField(Tags.IB_SLBAvailableForLoan);
                }
                public QuickFix.Fields.IB_SLBPrivateLocate IB_SLBPrivateLocate
                { 
                    get 
                    {
                        QuickFix.Fields.IB_SLBPrivateLocate val = new QuickFix.Fields.IB_SLBPrivateLocate();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_SLBPrivateLocate val) 
                { 
                    this.IB_SLBPrivateLocate = val;
                }
                
                public QuickFix.Fields.IB_SLBPrivateLocate Get(QuickFix.Fields.IB_SLBPrivateLocate val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_SLBPrivateLocate val) 
                { 
                    return IsSetIB_SLBPrivateLocate();
                }
                
                public bool IsSetIB_SLBPrivateLocate() 
                { 
                    return IsSetField(Tags.IB_SLBPrivateLocate);
                }
            
            }
        }
    }
}
