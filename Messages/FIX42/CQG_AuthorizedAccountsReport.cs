// This is a generated file.  Don't edit it directly!

using QuickFix.Fields;
namespace QuickFix
{
    namespace FIX42 
    {
        public class CQG_AuthorizedAccountsReport : Message
        {
            public const string MsgType = "UZR";

            public CQG_AuthorizedAccountsReport() : base()
            {
                this.Header.SetField(new QuickFix.Fields.MsgType("UZR"));
            }

            public CQG_AuthorizedAccountsReport(
                    QuickFix.Fields.CQG_RequestID aCQG_RequestID,
                    QuickFix.Fields.CQG_LastFragment aCQG_LastFragment
                ) : this()
            {
                this.CQG_RequestID = aCQG_RequestID;
                this.CQG_LastFragment = aCQG_LastFragment;
            }

            public QuickFix.Fields.CQG_RequestID CQG_RequestID
            { 
                get 
                {
                    QuickFix.Fields.CQG_RequestID val = new QuickFix.Fields.CQG_RequestID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_RequestID val) 
            { 
                this.CQG_RequestID = val;
            }
            
            public QuickFix.Fields.CQG_RequestID Get(QuickFix.Fields.CQG_RequestID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_RequestID val) 
            { 
                return IsSetCQG_RequestID();
            }
            
            public bool IsSetCQG_RequestID() 
            { 
                return IsSetField(Tags.CQG_RequestID);
            }
            public QuickFix.Fields.CQG_LastFragment CQG_LastFragment
            { 
                get 
                {
                    QuickFix.Fields.CQG_LastFragment val = new QuickFix.Fields.CQG_LastFragment();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_LastFragment val) 
            { 
                this.CQG_LastFragment = val;
            }
            
            public QuickFix.Fields.CQG_LastFragment Get(QuickFix.Fields.CQG_LastFragment val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_LastFragment val) 
            { 
                return IsSetCQG_LastFragment();
            }
            
            public bool IsSetCQG_LastFragment() 
            { 
                return IsSetField(Tags.CQG_LastFragment);
            }
            public QuickFix.Fields.CQG_NoAccounts CQG_NoAccounts
            { 
                get 
                {
                    QuickFix.Fields.CQG_NoAccounts val = new QuickFix.Fields.CQG_NoAccounts();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_NoAccounts val) 
            { 
                this.CQG_NoAccounts = val;
            }
            
            public QuickFix.Fields.CQG_NoAccounts Get(QuickFix.Fields.CQG_NoAccounts val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_NoAccounts val) 
            { 
                return IsSetCQG_NoAccounts();
            }
            
            public bool IsSetCQG_NoAccounts() 
            { 
                return IsSetField(Tags.CQG_NoAccounts);
            }
            public class CQG_NoAccountsGroup : Group
            {
                public static int[] fieldOrder = {Tags.Account, Tags.CQG_AccountName, Tags.CQG_FCMAccountNumber, Tags.CQG_FCMID, Tags.CQG_FCMName, Tags.CQG_ViewOnly, 0};
            
                public CQG_NoAccountsGroup() 
                  :base( Tags.CQG_NoAccounts, Tags.Account, fieldOrder)
                {
                }
            
                public override Group Clone()
                {
                    var clone = new CQG_NoAccountsGroup();
                    clone.CopyStateFrom(this);
                    return clone;
                }
            
                public QuickFix.Fields.Account Account
                { 
                    get 
                    {
                        QuickFix.Fields.Account val = new QuickFix.Fields.Account();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.Account val) 
                { 
                    this.Account = val;
                }
                
                public QuickFix.Fields.Account Get(QuickFix.Fields.Account val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.Account val) 
                { 
                    return IsSetAccount();
                }
                
                public bool IsSetAccount() 
                { 
                    return IsSetField(Tags.Account);
                }
                public QuickFix.Fields.CQG_AccountName CQG_AccountName
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_AccountName val = new QuickFix.Fields.CQG_AccountName();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_AccountName val) 
                { 
                    this.CQG_AccountName = val;
                }
                
                public QuickFix.Fields.CQG_AccountName Get(QuickFix.Fields.CQG_AccountName val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_AccountName val) 
                { 
                    return IsSetCQG_AccountName();
                }
                
                public bool IsSetCQG_AccountName() 
                { 
                    return IsSetField(Tags.CQG_AccountName);
                }
                public QuickFix.Fields.CQG_FCMAccountNumber CQG_FCMAccountNumber
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_FCMAccountNumber val = new QuickFix.Fields.CQG_FCMAccountNumber();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_FCMAccountNumber val) 
                { 
                    this.CQG_FCMAccountNumber = val;
                }
                
                public QuickFix.Fields.CQG_FCMAccountNumber Get(QuickFix.Fields.CQG_FCMAccountNumber val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_FCMAccountNumber val) 
                { 
                    return IsSetCQG_FCMAccountNumber();
                }
                
                public bool IsSetCQG_FCMAccountNumber() 
                { 
                    return IsSetField(Tags.CQG_FCMAccountNumber);
                }
                public QuickFix.Fields.CQG_FCMID CQG_FCMID
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_FCMID val = new QuickFix.Fields.CQG_FCMID();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_FCMID val) 
                { 
                    this.CQG_FCMID = val;
                }
                
                public QuickFix.Fields.CQG_FCMID Get(QuickFix.Fields.CQG_FCMID val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_FCMID val) 
                { 
                    return IsSetCQG_FCMID();
                }
                
                public bool IsSetCQG_FCMID() 
                { 
                    return IsSetField(Tags.CQG_FCMID);
                }
                public QuickFix.Fields.CQG_FCMName CQG_FCMName
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_FCMName val = new QuickFix.Fields.CQG_FCMName();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_FCMName val) 
                { 
                    this.CQG_FCMName = val;
                }
                
                public QuickFix.Fields.CQG_FCMName Get(QuickFix.Fields.CQG_FCMName val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_FCMName val) 
                { 
                    return IsSetCQG_FCMName();
                }
                
                public bool IsSetCQG_FCMName() 
                { 
                    return IsSetField(Tags.CQG_FCMName);
                }
                public QuickFix.Fields.CQG_ViewOnly CQG_ViewOnly
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_ViewOnly val = new QuickFix.Fields.CQG_ViewOnly();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_ViewOnly val) 
                { 
                    this.CQG_ViewOnly = val;
                }
                
                public QuickFix.Fields.CQG_ViewOnly Get(QuickFix.Fields.CQG_ViewOnly val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_ViewOnly val) 
                { 
                    return IsSetCQG_ViewOnly();
                }
                
                public bool IsSetCQG_ViewOnly() 
                { 
                    return IsSetField(Tags.CQG_ViewOnly);
                }
            
            }
        }
    }
}
