// This is a generated file.  Don't edit it directly!

using QuickFix.Fields;
namespace QuickFix
{
    namespace FIX42 
    {
        public class CQG_RequestForPosition : Message
        {
            public const string MsgType = "UAN";

            public CQG_RequestForPosition() : base()
            {
                this.Header.SetField(new QuickFix.Fields.MsgType("UAN"));
            }

            public CQG_RequestForPosition(
                    QuickFix.Fields.CQG_PosReqID aCQG_PosReqID,
                    QuickFix.Fields.CQG_PosReqType aCQG_PosReqType
                ) : this()
            {
                this.CQG_PosReqID = aCQG_PosReqID;
                this.CQG_PosReqType = aCQG_PosReqType;
            }

            public QuickFix.Fields.Account Account
            { 
                get 
                {
                    QuickFix.Fields.Account val = new QuickFix.Fields.Account();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.Account val) 
            { 
                this.Account = val;
            }
            
            public QuickFix.Fields.Account Get(QuickFix.Fields.Account val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.Account val) 
            { 
                return IsSetAccount();
            }
            
            public bool IsSetAccount() 
            { 
                return IsSetField(Tags.Account);
            }
            public QuickFix.Fields.SubscriptionRequestType SubscriptionRequestType
            { 
                get 
                {
                    QuickFix.Fields.SubscriptionRequestType val = new QuickFix.Fields.SubscriptionRequestType();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.SubscriptionRequestType val) 
            { 
                this.SubscriptionRequestType = val;
            }
            
            public QuickFix.Fields.SubscriptionRequestType Get(QuickFix.Fields.SubscriptionRequestType val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.SubscriptionRequestType val) 
            { 
                return IsSetSubscriptionRequestType();
            }
            
            public bool IsSetSubscriptionRequestType() 
            { 
                return IsSetField(Tags.SubscriptionRequestType);
            }
            public QuickFix.Fields.CQG_PosReqID CQG_PosReqID
            { 
                get 
                {
                    QuickFix.Fields.CQG_PosReqID val = new QuickFix.Fields.CQG_PosReqID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_PosReqID val) 
            { 
                this.CQG_PosReqID = val;
            }
            
            public QuickFix.Fields.CQG_PosReqID Get(QuickFix.Fields.CQG_PosReqID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_PosReqID val) 
            { 
                return IsSetCQG_PosReqID();
            }
            
            public bool IsSetCQG_PosReqID() 
            { 
                return IsSetField(Tags.CQG_PosReqID);
            }
            public QuickFix.Fields.CQG_PosReqType CQG_PosReqType
            { 
                get 
                {
                    QuickFix.Fields.CQG_PosReqType val = new QuickFix.Fields.CQG_PosReqType();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_PosReqType val) 
            { 
                this.CQG_PosReqType = val;
            }
            
            public QuickFix.Fields.CQG_PosReqType Get(QuickFix.Fields.CQG_PosReqType val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_PosReqType val) 
            { 
                return IsSetCQG_PosReqType();
            }
            
            public bool IsSetCQG_PosReqType() 
            { 
                return IsSetField(Tags.CQG_PosReqType);
            }
        }
    }
}
