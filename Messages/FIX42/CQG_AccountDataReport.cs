// This is a generated file.  Don't edit it directly!

using QuickFix.Fields;
namespace QuickFix
{
    namespace FIX42 
    {
        public class CQG_AccountDataReport : Message
        {
            public const string MsgType = "UAD";

            public CQG_AccountDataReport() : base()
            {
                this.Header.SetField(new QuickFix.Fields.MsgType("UAD"));
            }

            public CQG_AccountDataReport(
                    QuickFix.Fields.Account aAccount,
                    QuickFix.Fields.UnsolicitedIndicator aUnsolicitedIndicator,
                    QuickFix.Fields.CQG_AcctReqID aCQG_AcctReqID
                ) : this()
            {
                this.Account = aAccount;
                this.UnsolicitedIndicator = aUnsolicitedIndicator;
                this.CQG_AcctReqID = aCQG_AcctReqID;
            }

            public QuickFix.Fields.Account Account
            { 
                get 
                {
                    QuickFix.Fields.Account val = new QuickFix.Fields.Account();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.Account val) 
            { 
                this.Account = val;
            }
            
            public QuickFix.Fields.Account Get(QuickFix.Fields.Account val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.Account val) 
            { 
                return IsSetAccount();
            }
            
            public bool IsSetAccount() 
            { 
                return IsSetField(Tags.Account);
            }
            public QuickFix.Fields.Currency Currency
            { 
                get 
                {
                    QuickFix.Fields.Currency val = new QuickFix.Fields.Currency();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.Currency val) 
            { 
                this.Currency = val;
            }
            
            public QuickFix.Fields.Currency Get(QuickFix.Fields.Currency val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.Currency val) 
            { 
                return IsSetCurrency();
            }
            
            public bool IsSetCurrency() 
            { 
                return IsSetField(Tags.Currency);
            }
            public QuickFix.Fields.UnsolicitedIndicator UnsolicitedIndicator
            { 
                get 
                {
                    QuickFix.Fields.UnsolicitedIndicator val = new QuickFix.Fields.UnsolicitedIndicator();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.UnsolicitedIndicator val) 
            { 
                this.UnsolicitedIndicator = val;
            }
            
            public QuickFix.Fields.UnsolicitedIndicator Get(QuickFix.Fields.UnsolicitedIndicator val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.UnsolicitedIndicator val) 
            { 
                return IsSetUnsolicitedIndicator();
            }
            
            public bool IsSetUnsolicitedIndicator() 
            { 
                return IsSetField(Tags.UnsolicitedIndicator);
            }
            public QuickFix.Fields.CQG_AcctReqID CQG_AcctReqID
            { 
                get 
                {
                    QuickFix.Fields.CQG_AcctReqID val = new QuickFix.Fields.CQG_AcctReqID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_AcctReqID val) 
            { 
                this.CQG_AcctReqID = val;
            }
            
            public QuickFix.Fields.CQG_AcctReqID Get(QuickFix.Fields.CQG_AcctReqID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_AcctReqID val) 
            { 
                return IsSetCQG_AcctReqID();
            }
            
            public bool IsSetCQG_AcctReqID() 
            { 
                return IsSetField(Tags.CQG_AcctReqID);
            }
            public QuickFix.Fields.CQG_AccountName CQG_AccountName
            { 
                get 
                {
                    QuickFix.Fields.CQG_AccountName val = new QuickFix.Fields.CQG_AccountName();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_AccountName val) 
            { 
                this.CQG_AccountName = val;
            }
            
            public QuickFix.Fields.CQG_AccountName Get(QuickFix.Fields.CQG_AccountName val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_AccountName val) 
            { 
                return IsSetCQG_AccountName();
            }
            
            public bool IsSetCQG_AccountName() 
            { 
                return IsSetField(Tags.CQG_AccountName);
            }
            public QuickFix.Fields.CQG_AsOfDate CQG_AsOfDate
            { 
                get 
                {
                    QuickFix.Fields.CQG_AsOfDate val = new QuickFix.Fields.CQG_AsOfDate();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_AsOfDate val) 
            { 
                this.CQG_AsOfDate = val;
            }
            
            public QuickFix.Fields.CQG_AsOfDate Get(QuickFix.Fields.CQG_AsOfDate val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_AsOfDate val) 
            { 
                return IsSetCQG_AsOfDate();
            }
            
            public bool IsSetCQG_AsOfDate() 
            { 
                return IsSetField(Tags.CQG_AsOfDate);
            }
            public QuickFix.Fields.CQG_CashExcess CQG_CashExcess
            { 
                get 
                {
                    QuickFix.Fields.CQG_CashExcess val = new QuickFix.Fields.CQG_CashExcess();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_CashExcess val) 
            { 
                this.CQG_CashExcess = val;
            }
            
            public QuickFix.Fields.CQG_CashExcess Get(QuickFix.Fields.CQG_CashExcess val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_CashExcess val) 
            { 
                return IsSetCQG_CashExcess();
            }
            
            public bool IsSetCQG_CashExcess() 
            { 
                return IsSetField(Tags.CQG_CashExcess);
            }
            public QuickFix.Fields.CQG_CollateralOnDeposit CQG_CollateralOnDeposit
            { 
                get 
                {
                    QuickFix.Fields.CQG_CollateralOnDeposit val = new QuickFix.Fields.CQG_CollateralOnDeposit();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_CollateralOnDeposit val) 
            { 
                this.CQG_CollateralOnDeposit = val;
            }
            
            public QuickFix.Fields.CQG_CollateralOnDeposit Get(QuickFix.Fields.CQG_CollateralOnDeposit val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_CollateralOnDeposit val) 
            { 
                return IsSetCQG_CollateralOnDeposit();
            }
            
            public bool IsSetCQG_CollateralOnDeposit() 
            { 
                return IsSetField(Tags.CQG_CollateralOnDeposit);
            }
            public QuickFix.Fields.CQG_FCMAccountNumber CQG_FCMAccountNumber
            { 
                get 
                {
                    QuickFix.Fields.CQG_FCMAccountNumber val = new QuickFix.Fields.CQG_FCMAccountNumber();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_FCMAccountNumber val) 
            { 
                this.CQG_FCMAccountNumber = val;
            }
            
            public QuickFix.Fields.CQG_FCMAccountNumber Get(QuickFix.Fields.CQG_FCMAccountNumber val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_FCMAccountNumber val) 
            { 
                return IsSetCQG_FCMAccountNumber();
            }
            
            public bool IsSetCQG_FCMAccountNumber() 
            { 
                return IsSetField(Tags.CQG_FCMAccountNumber);
            }
            public QuickFix.Fields.CQG_InitialMarginReqs CQG_InitialMarginReqs
            { 
                get 
                {
                    QuickFix.Fields.CQG_InitialMarginReqs val = new QuickFix.Fields.CQG_InitialMarginReqs();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_InitialMarginReqs val) 
            { 
                this.CQG_InitialMarginReqs = val;
            }
            
            public QuickFix.Fields.CQG_InitialMarginReqs Get(QuickFix.Fields.CQG_InitialMarginReqs val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_InitialMarginReqs val) 
            { 
                return IsSetCQG_InitialMarginReqs();
            }
            
            public bool IsSetCQG_InitialMarginReqs() 
            { 
                return IsSetField(Tags.CQG_InitialMarginReqs);
            }
            public QuickFix.Fields.CQG_MaintenanceMarginReqs CQG_MaintenanceMarginReqs
            { 
                get 
                {
                    QuickFix.Fields.CQG_MaintenanceMarginReqs val = new QuickFix.Fields.CQG_MaintenanceMarginReqs();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_MaintenanceMarginReqs val) 
            { 
                this.CQG_MaintenanceMarginReqs = val;
            }
            
            public QuickFix.Fields.CQG_MaintenanceMarginReqs Get(QuickFix.Fields.CQG_MaintenanceMarginReqs val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_MaintenanceMarginReqs val) 
            { 
                return IsSetCQG_MaintenanceMarginReqs();
            }
            
            public bool IsSetCQG_MaintenanceMarginReqs() 
            { 
                return IsSetField(Tags.CQG_MaintenanceMarginReqs);
            }
            public QuickFix.Fields.CQG_MarketValue CQG_MarketValue
            { 
                get 
                {
                    QuickFix.Fields.CQG_MarketValue val = new QuickFix.Fields.CQG_MarketValue();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_MarketValue val) 
            { 
                this.CQG_MarketValue = val;
            }
            
            public QuickFix.Fields.CQG_MarketValue Get(QuickFix.Fields.CQG_MarketValue val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_MarketValue val) 
            { 
                return IsSetCQG_MarketValue();
            }
            
            public bool IsSetCQG_MarketValue() 
            { 
                return IsSetField(Tags.CQG_MarketValue);
            }
            public QuickFix.Fields.CQG_OpenTradeEquity CQG_OpenTradeEquity
            { 
                get 
                {
                    QuickFix.Fields.CQG_OpenTradeEquity val = new QuickFix.Fields.CQG_OpenTradeEquity();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_OpenTradeEquity val) 
            { 
                this.CQG_OpenTradeEquity = val;
            }
            
            public QuickFix.Fields.CQG_OpenTradeEquity Get(QuickFix.Fields.CQG_OpenTradeEquity val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_OpenTradeEquity val) 
            { 
                return IsSetCQG_OpenTradeEquity();
            }
            
            public bool IsSetCQG_OpenTradeEquity() 
            { 
                return IsSetField(Tags.CQG_OpenTradeEquity);
            }
            public QuickFix.Fields.CQG_TotalAccountValue CQG_TotalAccountValue
            { 
                get 
                {
                    QuickFix.Fields.CQG_TotalAccountValue val = new QuickFix.Fields.CQG_TotalAccountValue();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_TotalAccountValue val) 
            { 
                this.CQG_TotalAccountValue = val;
            }
            
            public QuickFix.Fields.CQG_TotalAccountValue Get(QuickFix.Fields.CQG_TotalAccountValue val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_TotalAccountValue val) 
            { 
                return IsSetCQG_TotalAccountValue();
            }
            
            public bool IsSetCQG_TotalAccountValue() 
            { 
                return IsSetField(Tags.CQG_TotalAccountValue);
            }
            public QuickFix.Fields.CQG_TotalEndingCashBalance CQG_TotalEndingCashBalance
            { 
                get 
                {
                    QuickFix.Fields.CQG_TotalEndingCashBalance val = new QuickFix.Fields.CQG_TotalEndingCashBalance();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_TotalEndingCashBalance val) 
            { 
                this.CQG_TotalEndingCashBalance = val;
            }
            
            public QuickFix.Fields.CQG_TotalEndingCashBalance Get(QuickFix.Fields.CQG_TotalEndingCashBalance val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_TotalEndingCashBalance val) 
            { 
                return IsSetCQG_TotalEndingCashBalance();
            }
            
            public bool IsSetCQG_TotalEndingCashBalance() 
            { 
                return IsSetField(Tags.CQG_TotalEndingCashBalance);
            }
            public QuickFix.Fields.CQG_UnrealizedPL CQG_UnrealizedPL
            { 
                get 
                {
                    QuickFix.Fields.CQG_UnrealizedPL val = new QuickFix.Fields.CQG_UnrealizedPL();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_UnrealizedPL val) 
            { 
                this.CQG_UnrealizedPL = val;
            }
            
            public QuickFix.Fields.CQG_UnrealizedPL Get(QuickFix.Fields.CQG_UnrealizedPL val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_UnrealizedPL val) 
            { 
                return IsSetCQG_UnrealizedPL();
            }
            
            public bool IsSetCQG_UnrealizedPL() 
            { 
                return IsSetField(Tags.CQG_UnrealizedPL);
            }
            public QuickFix.Fields.CQG_SalesSeriesNumber CQG_SalesSeriesNumber
            { 
                get 
                {
                    QuickFix.Fields.CQG_SalesSeriesNumber val = new QuickFix.Fields.CQG_SalesSeriesNumber();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_SalesSeriesNumber val) 
            { 
                this.CQG_SalesSeriesNumber = val;
            }
            
            public QuickFix.Fields.CQG_SalesSeriesNumber Get(QuickFix.Fields.CQG_SalesSeriesNumber val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_SalesSeriesNumber val) 
            { 
                return IsSetCQG_SalesSeriesNumber();
            }
            
            public bool IsSetCQG_SalesSeriesNumber() 
            { 
                return IsSetField(Tags.CQG_SalesSeriesNumber);
            }
            public QuickFix.Fields.CQG_LastRptRequested CQG_LastRptRequested
            { 
                get 
                {
                    QuickFix.Fields.CQG_LastRptRequested val = new QuickFix.Fields.CQG_LastRptRequested();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_LastRptRequested val) 
            { 
                this.CQG_LastRptRequested = val;
            }
            
            public QuickFix.Fields.CQG_LastRptRequested Get(QuickFix.Fields.CQG_LastRptRequested val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_LastRptRequested val) 
            { 
                return IsSetCQG_LastRptRequested();
            }
            
            public bool IsSetCQG_LastRptRequested() 
            { 
                return IsSetField(Tags.CQG_LastRptRequested);
            }
            public QuickFix.Fields.CQG_NoBalances CQG_NoBalances
            { 
                get 
                {
                    QuickFix.Fields.CQG_NoBalances val = new QuickFix.Fields.CQG_NoBalances();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_NoBalances val) 
            { 
                this.CQG_NoBalances = val;
            }
            
            public QuickFix.Fields.CQG_NoBalances Get(QuickFix.Fields.CQG_NoBalances val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_NoBalances val) 
            { 
                return IsSetCQG_NoBalances();
            }
            
            public bool IsSetCQG_NoBalances() 
            { 
                return IsSetField(Tags.CQG_NoBalances);
            }
            public class CQG_NoBalancesGroup : Group
            {
                public static int[] fieldOrder = {Tags.CQG_BalanceCurrency, Tags.CQG_BalanceEndingCashBalance, Tags.CQG_BalanceTotalAccountValue, Tags.CQG_BalanceOpenTradeEquity, Tags.CQG_BalanceUnrealizedPL, Tags.CQG_BalanceMarketValue, Tags.CQG_BalanceCashExcess, Tags.CQG_BalanceCollateralOnDeposit, Tags.CQG_BalanceInitialMarginReqs, Tags.CQG_BalanceMaintenanceMarginReqs, 0};
            
                public CQG_NoBalancesGroup() 
                  :base( Tags.CQG_NoBalances, Tags.CQG_BalanceCurrency, fieldOrder)
                {
                }
            
                public override Group Clone()
                {
                    var clone = new CQG_NoBalancesGroup();
                    clone.CopyStateFrom(this);
                    return clone;
                }
            
                public QuickFix.Fields.CQG_BalanceCurrency CQG_BalanceCurrency
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_BalanceCurrency val = new QuickFix.Fields.CQG_BalanceCurrency();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_BalanceCurrency val) 
                { 
                    this.CQG_BalanceCurrency = val;
                }
                
                public QuickFix.Fields.CQG_BalanceCurrency Get(QuickFix.Fields.CQG_BalanceCurrency val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_BalanceCurrency val) 
                { 
                    return IsSetCQG_BalanceCurrency();
                }
                
                public bool IsSetCQG_BalanceCurrency() 
                { 
                    return IsSetField(Tags.CQG_BalanceCurrency);
                }
                public QuickFix.Fields.CQG_BalanceEndingCashBalance CQG_BalanceEndingCashBalance
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_BalanceEndingCashBalance val = new QuickFix.Fields.CQG_BalanceEndingCashBalance();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_BalanceEndingCashBalance val) 
                { 
                    this.CQG_BalanceEndingCashBalance = val;
                }
                
                public QuickFix.Fields.CQG_BalanceEndingCashBalance Get(QuickFix.Fields.CQG_BalanceEndingCashBalance val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_BalanceEndingCashBalance val) 
                { 
                    return IsSetCQG_BalanceEndingCashBalance();
                }
                
                public bool IsSetCQG_BalanceEndingCashBalance() 
                { 
                    return IsSetField(Tags.CQG_BalanceEndingCashBalance);
                }
                public QuickFix.Fields.CQG_BalanceTotalAccountValue CQG_BalanceTotalAccountValue
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_BalanceTotalAccountValue val = new QuickFix.Fields.CQG_BalanceTotalAccountValue();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_BalanceTotalAccountValue val) 
                { 
                    this.CQG_BalanceTotalAccountValue = val;
                }
                
                public QuickFix.Fields.CQG_BalanceTotalAccountValue Get(QuickFix.Fields.CQG_BalanceTotalAccountValue val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_BalanceTotalAccountValue val) 
                { 
                    return IsSetCQG_BalanceTotalAccountValue();
                }
                
                public bool IsSetCQG_BalanceTotalAccountValue() 
                { 
                    return IsSetField(Tags.CQG_BalanceTotalAccountValue);
                }
                public QuickFix.Fields.CQG_BalanceOpenTradeEquity CQG_BalanceOpenTradeEquity
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_BalanceOpenTradeEquity val = new QuickFix.Fields.CQG_BalanceOpenTradeEquity();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_BalanceOpenTradeEquity val) 
                { 
                    this.CQG_BalanceOpenTradeEquity = val;
                }
                
                public QuickFix.Fields.CQG_BalanceOpenTradeEquity Get(QuickFix.Fields.CQG_BalanceOpenTradeEquity val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_BalanceOpenTradeEquity val) 
                { 
                    return IsSetCQG_BalanceOpenTradeEquity();
                }
                
                public bool IsSetCQG_BalanceOpenTradeEquity() 
                { 
                    return IsSetField(Tags.CQG_BalanceOpenTradeEquity);
                }
                public QuickFix.Fields.CQG_BalanceUnrealizedPL CQG_BalanceUnrealizedPL
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_BalanceUnrealizedPL val = new QuickFix.Fields.CQG_BalanceUnrealizedPL();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_BalanceUnrealizedPL val) 
                { 
                    this.CQG_BalanceUnrealizedPL = val;
                }
                
                public QuickFix.Fields.CQG_BalanceUnrealizedPL Get(QuickFix.Fields.CQG_BalanceUnrealizedPL val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_BalanceUnrealizedPL val) 
                { 
                    return IsSetCQG_BalanceUnrealizedPL();
                }
                
                public bool IsSetCQG_BalanceUnrealizedPL() 
                { 
                    return IsSetField(Tags.CQG_BalanceUnrealizedPL);
                }
                public QuickFix.Fields.CQG_BalanceMarketValue CQG_BalanceMarketValue
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_BalanceMarketValue val = new QuickFix.Fields.CQG_BalanceMarketValue();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_BalanceMarketValue val) 
                { 
                    this.CQG_BalanceMarketValue = val;
                }
                
                public QuickFix.Fields.CQG_BalanceMarketValue Get(QuickFix.Fields.CQG_BalanceMarketValue val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_BalanceMarketValue val) 
                { 
                    return IsSetCQG_BalanceMarketValue();
                }
                
                public bool IsSetCQG_BalanceMarketValue() 
                { 
                    return IsSetField(Tags.CQG_BalanceMarketValue);
                }
                public QuickFix.Fields.CQG_BalanceCashExcess CQG_BalanceCashExcess
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_BalanceCashExcess val = new QuickFix.Fields.CQG_BalanceCashExcess();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_BalanceCashExcess val) 
                { 
                    this.CQG_BalanceCashExcess = val;
                }
                
                public QuickFix.Fields.CQG_BalanceCashExcess Get(QuickFix.Fields.CQG_BalanceCashExcess val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_BalanceCashExcess val) 
                { 
                    return IsSetCQG_BalanceCashExcess();
                }
                
                public bool IsSetCQG_BalanceCashExcess() 
                { 
                    return IsSetField(Tags.CQG_BalanceCashExcess);
                }
                public QuickFix.Fields.CQG_BalanceCollateralOnDeposit CQG_BalanceCollateralOnDeposit
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_BalanceCollateralOnDeposit val = new QuickFix.Fields.CQG_BalanceCollateralOnDeposit();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_BalanceCollateralOnDeposit val) 
                { 
                    this.CQG_BalanceCollateralOnDeposit = val;
                }
                
                public QuickFix.Fields.CQG_BalanceCollateralOnDeposit Get(QuickFix.Fields.CQG_BalanceCollateralOnDeposit val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_BalanceCollateralOnDeposit val) 
                { 
                    return IsSetCQG_BalanceCollateralOnDeposit();
                }
                
                public bool IsSetCQG_BalanceCollateralOnDeposit() 
                { 
                    return IsSetField(Tags.CQG_BalanceCollateralOnDeposit);
                }
                public QuickFix.Fields.CQG_BalanceInitialMarginReqs CQG_BalanceInitialMarginReqs
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_BalanceInitialMarginReqs val = new QuickFix.Fields.CQG_BalanceInitialMarginReqs();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_BalanceInitialMarginReqs val) 
                { 
                    this.CQG_BalanceInitialMarginReqs = val;
                }
                
                public QuickFix.Fields.CQG_BalanceInitialMarginReqs Get(QuickFix.Fields.CQG_BalanceInitialMarginReqs val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_BalanceInitialMarginReqs val) 
                { 
                    return IsSetCQG_BalanceInitialMarginReqs();
                }
                
                public bool IsSetCQG_BalanceInitialMarginReqs() 
                { 
                    return IsSetField(Tags.CQG_BalanceInitialMarginReqs);
                }
                public QuickFix.Fields.CQG_BalanceMaintenanceMarginReqs CQG_BalanceMaintenanceMarginReqs
                { 
                    get 
                    {
                        QuickFix.Fields.CQG_BalanceMaintenanceMarginReqs val = new QuickFix.Fields.CQG_BalanceMaintenanceMarginReqs();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.CQG_BalanceMaintenanceMarginReqs val) 
                { 
                    this.CQG_BalanceMaintenanceMarginReqs = val;
                }
                
                public QuickFix.Fields.CQG_BalanceMaintenanceMarginReqs Get(QuickFix.Fields.CQG_BalanceMaintenanceMarginReqs val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.CQG_BalanceMaintenanceMarginReqs val) 
                { 
                    return IsSetCQG_BalanceMaintenanceMarginReqs();
                }
                
                public bool IsSetCQG_BalanceMaintenanceMarginReqs() 
                { 
                    return IsSetField(Tags.CQG_BalanceMaintenanceMarginReqs);
                }
            
            }
        }
    }
}
