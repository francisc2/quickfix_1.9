// This is a generated file.  Don't edit it directly!

using QuickFix.Fields;
namespace QuickFix
{
    namespace FIX42 
    {
        public class CQG_PositionReport : Message
        {
            public const string MsgType = "UAP";

            public CQG_PositionReport() : base()
            {
                this.Header.SetField(new QuickFix.Fields.MsgType("UAP"));
            }

            public CQG_PositionReport(
                    QuickFix.Fields.Account aAccount,
                    QuickFix.Fields.AvgPx aAvgPx,
                    QuickFix.Fields.Currency aCurrency,
                    QuickFix.Fields.LastShares aLastShares,
                    QuickFix.Fields.Side aSide,
                    QuickFix.Fields.Symbol aSymbol,
                    QuickFix.Fields.UnsolicitedIndicator aUnsolicitedIndicator,
                    QuickFix.Fields.CQG_ClearingBusinessDate aCQG_ClearingBusinessDate,
                    QuickFix.Fields.CQG_PosReqType aCQG_PosReqType
                ) : this()
            {
                this.Account = aAccount;
                this.AvgPx = aAvgPx;
                this.Currency = aCurrency;
                this.LastShares = aLastShares;
                this.Side = aSide;
                this.Symbol = aSymbol;
                this.UnsolicitedIndicator = aUnsolicitedIndicator;
                this.CQG_ClearingBusinessDate = aCQG_ClearingBusinessDate;
                this.CQG_PosReqType = aCQG_PosReqType;
            }

            public QuickFix.Fields.Account Account
            { 
                get 
                {
                    QuickFix.Fields.Account val = new QuickFix.Fields.Account();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.Account val) 
            { 
                this.Account = val;
            }
            
            public QuickFix.Fields.Account Get(QuickFix.Fields.Account val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.Account val) 
            { 
                return IsSetAccount();
            }
            
            public bool IsSetAccount() 
            { 
                return IsSetField(Tags.Account);
            }
            public QuickFix.Fields.AvgPx AvgPx
            { 
                get 
                {
                    QuickFix.Fields.AvgPx val = new QuickFix.Fields.AvgPx();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.AvgPx val) 
            { 
                this.AvgPx = val;
            }
            
            public QuickFix.Fields.AvgPx Get(QuickFix.Fields.AvgPx val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.AvgPx val) 
            { 
                return IsSetAvgPx();
            }
            
            public bool IsSetAvgPx() 
            { 
                return IsSetField(Tags.AvgPx);
            }
            public QuickFix.Fields.ClOrdID ClOrdID
            { 
                get 
                {
                    QuickFix.Fields.ClOrdID val = new QuickFix.Fields.ClOrdID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.ClOrdID val) 
            { 
                this.ClOrdID = val;
            }
            
            public QuickFix.Fields.ClOrdID Get(QuickFix.Fields.ClOrdID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.ClOrdID val) 
            { 
                return IsSetClOrdID();
            }
            
            public bool IsSetClOrdID() 
            { 
                return IsSetField(Tags.ClOrdID);
            }
            public QuickFix.Fields.Currency Currency
            { 
                get 
                {
                    QuickFix.Fields.Currency val = new QuickFix.Fields.Currency();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.Currency val) 
            { 
                this.Currency = val;
            }
            
            public QuickFix.Fields.Currency Get(QuickFix.Fields.Currency val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.Currency val) 
            { 
                return IsSetCurrency();
            }
            
            public bool IsSetCurrency() 
            { 
                return IsSetField(Tags.Currency);
            }
            public QuickFix.Fields.ExecID ExecID
            { 
                get 
                {
                    QuickFix.Fields.ExecID val = new QuickFix.Fields.ExecID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.ExecID val) 
            { 
                this.ExecID = val;
            }
            
            public QuickFix.Fields.ExecID Get(QuickFix.Fields.ExecID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.ExecID val) 
            { 
                return IsSetExecID();
            }
            
            public bool IsSetExecID() 
            { 
                return IsSetField(Tags.ExecID);
            }
            public QuickFix.Fields.IDSource IDSource
            { 
                get 
                {
                    QuickFix.Fields.IDSource val = new QuickFix.Fields.IDSource();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.IDSource val) 
            { 
                this.IDSource = val;
            }
            
            public QuickFix.Fields.IDSource Get(QuickFix.Fields.IDSource val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.IDSource val) 
            { 
                return IsSetIDSource();
            }
            
            public bool IsSetIDSource() 
            { 
                return IsSetField(Tags.IDSource);
            }
            public QuickFix.Fields.LastMkt LastMkt
            { 
                get 
                {
                    QuickFix.Fields.LastMkt val = new QuickFix.Fields.LastMkt();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.LastMkt val) 
            { 
                this.LastMkt = val;
            }
            
            public QuickFix.Fields.LastMkt Get(QuickFix.Fields.LastMkt val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.LastMkt val) 
            { 
                return IsSetLastMkt();
            }
            
            public bool IsSetLastMkt() 
            { 
                return IsSetField(Tags.LastMkt);
            }
            public QuickFix.Fields.LastShares LastShares
            { 
                get 
                {
                    QuickFix.Fields.LastShares val = new QuickFix.Fields.LastShares();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.LastShares val) 
            { 
                this.LastShares = val;
            }
            
            public QuickFix.Fields.LastShares Get(QuickFix.Fields.LastShares val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.LastShares val) 
            { 
                return IsSetLastShares();
            }
            
            public bool IsSetLastShares() 
            { 
                return IsSetField(Tags.LastShares);
            }
            public QuickFix.Fields.OrderID OrderID
            { 
                get 
                {
                    QuickFix.Fields.OrderID val = new QuickFix.Fields.OrderID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.OrderID val) 
            { 
                this.OrderID = val;
            }
            
            public QuickFix.Fields.OrderID Get(QuickFix.Fields.OrderID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.OrderID val) 
            { 
                return IsSetOrderID();
            }
            
            public bool IsSetOrderID() 
            { 
                return IsSetField(Tags.OrderID);
            }
            public QuickFix.Fields.SecurityID SecurityID
            { 
                get 
                {
                    QuickFix.Fields.SecurityID val = new QuickFix.Fields.SecurityID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.SecurityID val) 
            { 
                this.SecurityID = val;
            }
            
            public QuickFix.Fields.SecurityID Get(QuickFix.Fields.SecurityID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.SecurityID val) 
            { 
                return IsSetSecurityID();
            }
            
            public bool IsSetSecurityID() 
            { 
                return IsSetField(Tags.SecurityID);
            }
            public QuickFix.Fields.Side Side
            { 
                get 
                {
                    QuickFix.Fields.Side val = new QuickFix.Fields.Side();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.Side val) 
            { 
                this.Side = val;
            }
            
            public QuickFix.Fields.Side Get(QuickFix.Fields.Side val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.Side val) 
            { 
                return IsSetSide();
            }
            
            public bool IsSetSide() 
            { 
                return IsSetField(Tags.Side);
            }
            public QuickFix.Fields.Symbol Symbol
            { 
                get 
                {
                    QuickFix.Fields.Symbol val = new QuickFix.Fields.Symbol();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.Symbol val) 
            { 
                this.Symbol = val;
            }
            
            public QuickFix.Fields.Symbol Get(QuickFix.Fields.Symbol val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.Symbol val) 
            { 
                return IsSetSymbol();
            }
            
            public bool IsSetSymbol() 
            { 
                return IsSetField(Tags.Symbol);
            }
            public QuickFix.Fields.Text Text
            { 
                get 
                {
                    QuickFix.Fields.Text val = new QuickFix.Fields.Text();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.Text val) 
            { 
                this.Text = val;
            }
            
            public QuickFix.Fields.Text Get(QuickFix.Fields.Text val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.Text val) 
            { 
                return IsSetText();
            }
            
            public bool IsSetText() 
            { 
                return IsSetField(Tags.Text);
            }
            public QuickFix.Fields.TransactTime TransactTime
            { 
                get 
                {
                    QuickFix.Fields.TransactTime val = new QuickFix.Fields.TransactTime();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.TransactTime val) 
            { 
                this.TransactTime = val;
            }
            
            public QuickFix.Fields.TransactTime Get(QuickFix.Fields.TransactTime val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.TransactTime val) 
            { 
                return IsSetTransactTime();
            }
            
            public bool IsSetTransactTime() 
            { 
                return IsSetField(Tags.TransactTime);
            }
            public QuickFix.Fields.SymbolSfx SymbolSfx
            { 
                get 
                {
                    QuickFix.Fields.SymbolSfx val = new QuickFix.Fields.SymbolSfx();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.SymbolSfx val) 
            { 
                this.SymbolSfx = val;
            }
            
            public QuickFix.Fields.SymbolSfx Get(QuickFix.Fields.SymbolSfx val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.SymbolSfx val) 
            { 
                return IsSetSymbolSfx();
            }
            
            public bool IsSetSymbolSfx() 
            { 
                return IsSetField(Tags.SymbolSfx);
            }
            public QuickFix.Fields.ListID ListID
            { 
                get 
                {
                    QuickFix.Fields.ListID val = new QuickFix.Fields.ListID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.ListID val) 
            { 
                this.ListID = val;
            }
            
            public QuickFix.Fields.ListID Get(QuickFix.Fields.ListID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.ListID val) 
            { 
                return IsSetListID();
            }
            
            public bool IsSetListID() 
            { 
                return IsSetField(Tags.ListID);
            }
            public QuickFix.Fields.OnBehalfOfCompID OnBehalfOfCompID
            { 
                get 
                {
                    QuickFix.Fields.OnBehalfOfCompID val = new QuickFix.Fields.OnBehalfOfCompID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.OnBehalfOfCompID val) 
            { 
                this.OnBehalfOfCompID = val;
            }
            
            public QuickFix.Fields.OnBehalfOfCompID Get(QuickFix.Fields.OnBehalfOfCompID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.OnBehalfOfCompID val) 
            { 
                return IsSetOnBehalfOfCompID();
            }
            
            public bool IsSetOnBehalfOfCompID() 
            { 
                return IsSetField(Tags.OnBehalfOfCompID);
            }
            public QuickFix.Fields.DeliverToCompID DeliverToCompID
            { 
                get 
                {
                    QuickFix.Fields.DeliverToCompID val = new QuickFix.Fields.DeliverToCompID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.DeliverToCompID val) 
            { 
                this.DeliverToCompID = val;
            }
            
            public QuickFix.Fields.DeliverToCompID Get(QuickFix.Fields.DeliverToCompID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.DeliverToCompID val) 
            { 
                return IsSetDeliverToCompID();
            }
            
            public bool IsSetDeliverToCompID() 
            { 
                return IsSetField(Tags.DeliverToCompID);
            }
            public QuickFix.Fields.DeliverToSubID DeliverToSubID
            { 
                get 
                {
                    QuickFix.Fields.DeliverToSubID val = new QuickFix.Fields.DeliverToSubID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.DeliverToSubID val) 
            { 
                this.DeliverToSubID = val;
            }
            
            public QuickFix.Fields.DeliverToSubID Get(QuickFix.Fields.DeliverToSubID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.DeliverToSubID val) 
            { 
                return IsSetDeliverToSubID();
            }
            
            public bool IsSetDeliverToSubID() 
            { 
                return IsSetField(Tags.DeliverToSubID);
            }
            public QuickFix.Fields.SecurityType SecurityType
            { 
                get 
                {
                    QuickFix.Fields.SecurityType val = new QuickFix.Fields.SecurityType();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.SecurityType val) 
            { 
                this.SecurityType = val;
            }
            
            public QuickFix.Fields.SecurityType Get(QuickFix.Fields.SecurityType val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.SecurityType val) 
            { 
                return IsSetSecurityType();
            }
            
            public bool IsSetSecurityType() 
            { 
                return IsSetField(Tags.SecurityType);
            }
            public QuickFix.Fields.SecondaryOrderID SecondaryOrderID
            { 
                get 
                {
                    QuickFix.Fields.SecondaryOrderID val = new QuickFix.Fields.SecondaryOrderID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.SecondaryOrderID val) 
            { 
                this.SecondaryOrderID = val;
            }
            
            public QuickFix.Fields.SecondaryOrderID Get(QuickFix.Fields.SecondaryOrderID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.SecondaryOrderID val) 
            { 
                return IsSetSecondaryOrderID();
            }
            
            public bool IsSetSecondaryOrderID() 
            { 
                return IsSetField(Tags.SecondaryOrderID);
            }
            public QuickFix.Fields.MaturityMonthYear MaturityMonthYear
            { 
                get 
                {
                    QuickFix.Fields.MaturityMonthYear val = new QuickFix.Fields.MaturityMonthYear();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.MaturityMonthYear val) 
            { 
                this.MaturityMonthYear = val;
            }
            
            public QuickFix.Fields.MaturityMonthYear Get(QuickFix.Fields.MaturityMonthYear val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.MaturityMonthYear val) 
            { 
                return IsSetMaturityMonthYear();
            }
            
            public bool IsSetMaturityMonthYear() 
            { 
                return IsSetField(Tags.MaturityMonthYear);
            }
            public QuickFix.Fields.PutOrCall PutOrCall
            { 
                get 
                {
                    QuickFix.Fields.PutOrCall val = new QuickFix.Fields.PutOrCall();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.PutOrCall val) 
            { 
                this.PutOrCall = val;
            }
            
            public QuickFix.Fields.PutOrCall Get(QuickFix.Fields.PutOrCall val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.PutOrCall val) 
            { 
                return IsSetPutOrCall();
            }
            
            public bool IsSetPutOrCall() 
            { 
                return IsSetField(Tags.PutOrCall);
            }
            public QuickFix.Fields.StrikePrice StrikePrice
            { 
                get 
                {
                    QuickFix.Fields.StrikePrice val = new QuickFix.Fields.StrikePrice();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.StrikePrice val) 
            { 
                this.StrikePrice = val;
            }
            
            public QuickFix.Fields.StrikePrice Get(QuickFix.Fields.StrikePrice val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.StrikePrice val) 
            { 
                return IsSetStrikePrice();
            }
            
            public bool IsSetStrikePrice() 
            { 
                return IsSetField(Tags.StrikePrice);
            }
            public QuickFix.Fields.MaturityDay MaturityDay
            { 
                get 
                {
                    QuickFix.Fields.MaturityDay val = new QuickFix.Fields.MaturityDay();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.MaturityDay val) 
            { 
                this.MaturityDay = val;
            }
            
            public QuickFix.Fields.MaturityDay Get(QuickFix.Fields.MaturityDay val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.MaturityDay val) 
            { 
                return IsSetMaturityDay();
            }
            
            public bool IsSetMaturityDay() 
            { 
                return IsSetField(Tags.MaturityDay);
            }
            public QuickFix.Fields.SecurityExchange SecurityExchange
            { 
                get 
                {
                    QuickFix.Fields.SecurityExchange val = new QuickFix.Fields.SecurityExchange();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.SecurityExchange val) 
            { 
                this.SecurityExchange = val;
            }
            
            public QuickFix.Fields.SecurityExchange Get(QuickFix.Fields.SecurityExchange val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.SecurityExchange val) 
            { 
                return IsSetSecurityExchange();
            }
            
            public bool IsSetSecurityExchange() 
            { 
                return IsSetField(Tags.SecurityExchange);
            }
            public QuickFix.Fields.CouponRate CouponRate
            { 
                get 
                {
                    QuickFix.Fields.CouponRate val = new QuickFix.Fields.CouponRate();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CouponRate val) 
            { 
                this.CouponRate = val;
            }
            
            public QuickFix.Fields.CouponRate Get(QuickFix.Fields.CouponRate val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CouponRate val) 
            { 
                return IsSetCouponRate();
            }
            
            public bool IsSetCouponRate() 
            { 
                return IsSetField(Tags.CouponRate);
            }
            public QuickFix.Fields.UnsolicitedIndicator UnsolicitedIndicator
            { 
                get 
                {
                    QuickFix.Fields.UnsolicitedIndicator val = new QuickFix.Fields.UnsolicitedIndicator();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.UnsolicitedIndicator val) 
            { 
                this.UnsolicitedIndicator = val;
            }
            
            public QuickFix.Fields.UnsolicitedIndicator Get(QuickFix.Fields.UnsolicitedIndicator val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.UnsolicitedIndicator val) 
            { 
                return IsSetUnsolicitedIndicator();
            }
            
            public bool IsSetUnsolicitedIndicator() 
            { 
                return IsSetField(Tags.UnsolicitedIndicator);
            }
            public QuickFix.Fields.MaturityDate MaturityDate
            { 
                get 
                {
                    QuickFix.Fields.MaturityDate val = new QuickFix.Fields.MaturityDate();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.MaturityDate val) 
            { 
                this.MaturityDate = val;
            }
            
            public QuickFix.Fields.MaturityDate Get(QuickFix.Fields.MaturityDate val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.MaturityDate val) 
            { 
                return IsSetMaturityDate();
            }
            
            public bool IsSetMaturityDate() 
            { 
                return IsSetField(Tags.MaturityDate);
            }
            public QuickFix.Fields.CQG_CFICode CQG_CFICode
            { 
                get 
                {
                    QuickFix.Fields.CQG_CFICode val = new QuickFix.Fields.CQG_CFICode();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_CFICode val) 
            { 
                this.CQG_CFICode = val;
            }
            
            public QuickFix.Fields.CQG_CFICode Get(QuickFix.Fields.CQG_CFICode val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_CFICode val) 
            { 
                return IsSetCQG_CFICode();
            }
            
            public bool IsSetCQG_CFICode() 
            { 
                return IsSetField(Tags.CQG_CFICode);
            }
            public QuickFix.Fields.CQG_SecuritySubType CQG_SecuritySubType
            { 
                get 
                {
                    QuickFix.Fields.CQG_SecuritySubType val = new QuickFix.Fields.CQG_SecuritySubType();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_SecuritySubType val) 
            { 
                this.CQG_SecuritySubType = val;
            }
            
            public QuickFix.Fields.CQG_SecuritySubType Get(QuickFix.Fields.CQG_SecuritySubType val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_SecuritySubType val) 
            { 
                return IsSetCQG_SecuritySubType();
            }
            
            public bool IsSetCQG_SecuritySubType() 
            { 
                return IsSetField(Tags.CQG_SecuritySubType);
            }
            public QuickFix.Fields.CQG_AccountName CQG_AccountName
            { 
                get 
                {
                    QuickFix.Fields.CQG_AccountName val = new QuickFix.Fields.CQG_AccountName();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_AccountName val) 
            { 
                this.CQG_AccountName = val;
            }
            
            public QuickFix.Fields.CQG_AccountName Get(QuickFix.Fields.CQG_AccountName val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_AccountName val) 
            { 
                return IsSetCQG_AccountName();
            }
            
            public bool IsSetCQG_AccountName() 
            { 
                return IsSetField(Tags.CQG_AccountName);
            }
            public QuickFix.Fields.CQG_FCMAccountNumber CQG_FCMAccountNumber
            { 
                get 
                {
                    QuickFix.Fields.CQG_FCMAccountNumber val = new QuickFix.Fields.CQG_FCMAccountNumber();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_FCMAccountNumber val) 
            { 
                this.CQG_FCMAccountNumber = val;
            }
            
            public QuickFix.Fields.CQG_FCMAccountNumber Get(QuickFix.Fields.CQG_FCMAccountNumber val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_FCMAccountNumber val) 
            { 
                return IsSetCQG_FCMAccountNumber();
            }
            
            public bool IsSetCQG_FCMAccountNumber() 
            { 
                return IsSetField(Tags.CQG_FCMAccountNumber);
            }
            public QuickFix.Fields.CQG_OrderSource CQG_OrderSource
            { 
                get 
                {
                    QuickFix.Fields.CQG_OrderSource val = new QuickFix.Fields.CQG_OrderSource();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_OrderSource val) 
            { 
                this.CQG_OrderSource = val;
            }
            
            public QuickFix.Fields.CQG_OrderSource Get(QuickFix.Fields.CQG_OrderSource val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_OrderSource val) 
            { 
                return IsSetCQG_OrderSource();
            }
            
            public bool IsSetCQG_OrderSource() 
            { 
                return IsSetField(Tags.CQG_OrderSource);
            }
            public QuickFix.Fields.CQG_TradeID CQG_TradeID
            { 
                get 
                {
                    QuickFix.Fields.CQG_TradeID val = new QuickFix.Fields.CQG_TradeID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_TradeID val) 
            { 
                this.CQG_TradeID = val;
            }
            
            public QuickFix.Fields.CQG_TradeID Get(QuickFix.Fields.CQG_TradeID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_TradeID val) 
            { 
                return IsSetCQG_TradeID();
            }
            
            public bool IsSetCQG_TradeID() 
            { 
                return IsSetField(Tags.CQG_TradeID);
            }
            public QuickFix.Fields.CQG_ChainOrderID CQG_ChainOrderID
            { 
                get 
                {
                    QuickFix.Fields.CQG_ChainOrderID val = new QuickFix.Fields.CQG_ChainOrderID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_ChainOrderID val) 
            { 
                this.CQG_ChainOrderID = val;
            }
            
            public QuickFix.Fields.CQG_ChainOrderID Get(QuickFix.Fields.CQG_ChainOrderID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_ChainOrderID val) 
            { 
                return IsSetCQG_ChainOrderID();
            }
            
            public bool IsSetCQG_ChainOrderID() 
            { 
                return IsSetField(Tags.CQG_ChainOrderID);
            }
            public QuickFix.Fields.CQG_ListID CQG_ListID
            { 
                get 
                {
                    QuickFix.Fields.CQG_ListID val = new QuickFix.Fields.CQG_ListID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_ListID val) 
            { 
                this.CQG_ListID = val;
            }
            
            public QuickFix.Fields.CQG_ListID Get(QuickFix.Fields.CQG_ListID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_ListID val) 
            { 
                return IsSetCQG_ListID();
            }
            
            public bool IsSetCQG_ListID() 
            { 
                return IsSetField(Tags.CQG_ListID);
            }
            public QuickFix.Fields.CQG_OrderPlacementTime CQG_OrderPlacementTime
            { 
                get 
                {
                    QuickFix.Fields.CQG_OrderPlacementTime val = new QuickFix.Fields.CQG_OrderPlacementTime();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_OrderPlacementTime val) 
            { 
                this.CQG_OrderPlacementTime = val;
            }
            
            public QuickFix.Fields.CQG_OrderPlacementTime Get(QuickFix.Fields.CQG_OrderPlacementTime val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_OrderPlacementTime val) 
            { 
                return IsSetCQG_OrderPlacementTime();
            }
            
            public bool IsSetCQG_OrderPlacementTime() 
            { 
                return IsSetField(Tags.CQG_OrderPlacementTime);
            }
            public QuickFix.Fields.CQG_OrderCheckMark CQG_OrderCheckMark
            { 
                get 
                {
                    QuickFix.Fields.CQG_OrderCheckMark val = new QuickFix.Fields.CQG_OrderCheckMark();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_OrderCheckMark val) 
            { 
                this.CQG_OrderCheckMark = val;
            }
            
            public QuickFix.Fields.CQG_OrderCheckMark Get(QuickFix.Fields.CQG_OrderCheckMark val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_OrderCheckMark val) 
            { 
                return IsSetCQG_OrderCheckMark();
            }
            
            public bool IsSetCQG_OrderCheckMark() 
            { 
                return IsSetField(Tags.CQG_OrderCheckMark);
            }
            public QuickFix.Fields.CQG_SecondaryClOrderID CQG_SecondaryClOrderID
            { 
                get 
                {
                    QuickFix.Fields.CQG_SecondaryClOrderID val = new QuickFix.Fields.CQG_SecondaryClOrderID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_SecondaryClOrderID val) 
            { 
                this.CQG_SecondaryClOrderID = val;
            }
            
            public QuickFix.Fields.CQG_SecondaryClOrderID Get(QuickFix.Fields.CQG_SecondaryClOrderID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_SecondaryClOrderID val) 
            { 
                return IsSetCQG_SecondaryClOrderID();
            }
            
            public bool IsSetCQG_SecondaryClOrderID() 
            { 
                return IsSetField(Tags.CQG_SecondaryClOrderID);
            }
            public QuickFix.Fields.CQG_MarketValue CQG_MarketValue
            { 
                get 
                {
                    QuickFix.Fields.CQG_MarketValue val = new QuickFix.Fields.CQG_MarketValue();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_MarketValue val) 
            { 
                this.CQG_MarketValue = val;
            }
            
            public QuickFix.Fields.CQG_MarketValue Get(QuickFix.Fields.CQG_MarketValue val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_MarketValue val) 
            { 
                return IsSetCQG_MarketValue();
            }
            
            public bool IsSetCQG_MarketValue() 
            { 
                return IsSetField(Tags.CQG_MarketValue);
            }
            public QuickFix.Fields.CQG_OpenTradeEquity CQG_OpenTradeEquity
            { 
                get 
                {
                    QuickFix.Fields.CQG_OpenTradeEquity val = new QuickFix.Fields.CQG_OpenTradeEquity();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_OpenTradeEquity val) 
            { 
                this.CQG_OpenTradeEquity = val;
            }
            
            public QuickFix.Fields.CQG_OpenTradeEquity Get(QuickFix.Fields.CQG_OpenTradeEquity val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_OpenTradeEquity val) 
            { 
                return IsSetCQG_OpenTradeEquity();
            }
            
            public bool IsSetCQG_OpenTradeEquity() 
            { 
                return IsSetField(Tags.CQG_OpenTradeEquity);
            }
            public QuickFix.Fields.CQG_UnrealizedPL CQG_UnrealizedPL
            { 
                get 
                {
                    QuickFix.Fields.CQG_UnrealizedPL val = new QuickFix.Fields.CQG_UnrealizedPL();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_UnrealizedPL val) 
            { 
                this.CQG_UnrealizedPL = val;
            }
            
            public QuickFix.Fields.CQG_UnrealizedPL Get(QuickFix.Fields.CQG_UnrealizedPL val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_UnrealizedPL val) 
            { 
                return IsSetCQG_UnrealizedPL();
            }
            
            public bool IsSetCQG_UnrealizedPL() 
            { 
                return IsSetField(Tags.CQG_UnrealizedPL);
            }
            public QuickFix.Fields.CQG_StatementDate CQG_StatementDate
            { 
                get 
                {
                    QuickFix.Fields.CQG_StatementDate val = new QuickFix.Fields.CQG_StatementDate();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_StatementDate val) 
            { 
                this.CQG_StatementDate = val;
            }
            
            public QuickFix.Fields.CQG_StatementDate Get(QuickFix.Fields.CQG_StatementDate val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_StatementDate val) 
            { 
                return IsSetCQG_StatementDate();
            }
            
            public bool IsSetCQG_StatementDate() 
            { 
                return IsSetField(Tags.CQG_StatementDate);
            }
            public QuickFix.Fields.CQG_SalesSeriesNumber CQG_SalesSeriesNumber
            { 
                get 
                {
                    QuickFix.Fields.CQG_SalesSeriesNumber val = new QuickFix.Fields.CQG_SalesSeriesNumber();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_SalesSeriesNumber val) 
            { 
                this.CQG_SalesSeriesNumber = val;
            }
            
            public QuickFix.Fields.CQG_SalesSeriesNumber Get(QuickFix.Fields.CQG_SalesSeriesNumber val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_SalesSeriesNumber val) 
            { 
                return IsSetCQG_SalesSeriesNumber();
            }
            
            public bool IsSetCQG_SalesSeriesNumber() 
            { 
                return IsSetField(Tags.CQG_SalesSeriesNumber);
            }
            public QuickFix.Fields.CQG_ClearingBusinessDate CQG_ClearingBusinessDate
            { 
                get 
                {
                    QuickFix.Fields.CQG_ClearingBusinessDate val = new QuickFix.Fields.CQG_ClearingBusinessDate();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_ClearingBusinessDate val) 
            { 
                this.CQG_ClearingBusinessDate = val;
            }
            
            public QuickFix.Fields.CQG_ClearingBusinessDate Get(QuickFix.Fields.CQG_ClearingBusinessDate val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_ClearingBusinessDate val) 
            { 
                return IsSetCQG_ClearingBusinessDate();
            }
            
            public bool IsSetCQG_ClearingBusinessDate() 
            { 
                return IsSetField(Tags.CQG_ClearingBusinessDate);
            }
            public QuickFix.Fields.CQG_PosReqType CQG_PosReqType
            { 
                get 
                {
                    QuickFix.Fields.CQG_PosReqType val = new QuickFix.Fields.CQG_PosReqType();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_PosReqType val) 
            { 
                this.CQG_PosReqType = val;
            }
            
            public QuickFix.Fields.CQG_PosReqType Get(QuickFix.Fields.CQG_PosReqType val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_PosReqType val) 
            { 
                return IsSetCQG_PosReqType();
            }
            
            public bool IsSetCQG_PosReqType() 
            { 
                return IsSetField(Tags.CQG_PosReqType);
            }
            public QuickFix.Fields.CQG_SettlPrice CQG_SettlPrice
            { 
                get 
                {
                    QuickFix.Fields.CQG_SettlPrice val = new QuickFix.Fields.CQG_SettlPrice();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_SettlPrice val) 
            { 
                this.CQG_SettlPrice = val;
            }
            
            public QuickFix.Fields.CQG_SettlPrice Get(QuickFix.Fields.CQG_SettlPrice val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_SettlPrice val) 
            { 
                return IsSetCQG_SettlPrice();
            }
            
            public bool IsSetCQG_SettlPrice() 
            { 
                return IsSetField(Tags.CQG_SettlPrice);
            }
            public QuickFix.Fields.CQG_PosReqID CQG_PosReqID
            { 
                get 
                {
                    QuickFix.Fields.CQG_PosReqID val = new QuickFix.Fields.CQG_PosReqID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_PosReqID val) 
            { 
                this.CQG_PosReqID = val;
            }
            
            public QuickFix.Fields.CQG_PosReqID Get(QuickFix.Fields.CQG_PosReqID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_PosReqID val) 
            { 
                return IsSetCQG_PosReqID();
            }
            
            public bool IsSetCQG_PosReqID() 
            { 
                return IsSetField(Tags.CQG_PosReqID);
            }
        }
    }
}
