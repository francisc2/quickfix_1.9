// This is a generated file.  Don't edit it directly!

using QuickFix.Fields;
namespace QuickFix
{
    namespace FIX42 
    {
        public class IB_AccountInfo : Message
        {
            public const string MsgType = "UT";

            public IB_AccountInfo() : base()
            {
                this.Header.SetField(new QuickFix.Fields.MsgType("UT"));
            }

            public IB_AccountInfo(
                    QuickFix.Fields.IB_ReqID aIB_ReqID
                ) : this()
            {
                this.IB_ReqID = aIB_ReqID;
            }

            public QuickFix.Fields.IB_ReqID IB_ReqID
            { 
                get 
                {
                    QuickFix.Fields.IB_ReqID val = new QuickFix.Fields.IB_ReqID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.IB_ReqID val) 
            { 
                this.IB_ReqID = val;
            }
            
            public QuickFix.Fields.IB_ReqID Get(QuickFix.Fields.IB_ReqID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.IB_ReqID val) 
            { 
                return IsSetIB_ReqID();
            }
            
            public bool IsSetIB_ReqID() 
            { 
                return IsSetField(Tags.IB_ReqID);
            }
            public QuickFix.Fields.IB_FirstKey IB_FirstKey
            { 
                get 
                {
                    QuickFix.Fields.IB_FirstKey val = new QuickFix.Fields.IB_FirstKey();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.IB_FirstKey val) 
            { 
                this.IB_FirstKey = val;
            }
            
            public QuickFix.Fields.IB_FirstKey Get(QuickFix.Fields.IB_FirstKey val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.IB_FirstKey val) 
            { 
                return IsSetIB_FirstKey();
            }
            
            public bool IsSetIB_FirstKey() 
            { 
                return IsSetField(Tags.IB_FirstKey);
            }
            public class IB_FirstKeyGroup : Group
            {
                public static int[] fieldOrder = {Tags.IB_TimeStamp, Tags.IB_Value, Tags.Currency, Tags.IB_EventSeverity, 0};
            
                public IB_FirstKeyGroup() 
                  :base( Tags.IB_FirstKey, Tags.IB_TimeStamp, fieldOrder)
                {
                }
            
                public override Group Clone()
                {
                    var clone = new IB_FirstKeyGroup();
                    clone.CopyStateFrom(this);
                    return clone;
                }
            
                public QuickFix.Fields.IB_TimeStamp IB_TimeStamp
                { 
                    get 
                    {
                        QuickFix.Fields.IB_TimeStamp val = new QuickFix.Fields.IB_TimeStamp();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_TimeStamp val) 
                { 
                    this.IB_TimeStamp = val;
                }
                
                public QuickFix.Fields.IB_TimeStamp Get(QuickFix.Fields.IB_TimeStamp val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_TimeStamp val) 
                { 
                    return IsSetIB_TimeStamp();
                }
                
                public bool IsSetIB_TimeStamp() 
                { 
                    return IsSetField(Tags.IB_TimeStamp);
                }
                public QuickFix.Fields.IB_Value IB_Value
                { 
                    get 
                    {
                        QuickFix.Fields.IB_Value val = new QuickFix.Fields.IB_Value();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_Value val) 
                { 
                    this.IB_Value = val;
                }
                
                public QuickFix.Fields.IB_Value Get(QuickFix.Fields.IB_Value val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_Value val) 
                { 
                    return IsSetIB_Value();
                }
                
                public bool IsSetIB_Value() 
                { 
                    return IsSetField(Tags.IB_Value);
                }
                public QuickFix.Fields.Currency Currency
                { 
                    get 
                    {
                        QuickFix.Fields.Currency val = new QuickFix.Fields.Currency();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.Currency val) 
                { 
                    this.Currency = val;
                }
                
                public QuickFix.Fields.Currency Get(QuickFix.Fields.Currency val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.Currency val) 
                { 
                    return IsSetCurrency();
                }
                
                public bool IsSetCurrency() 
                { 
                    return IsSetField(Tags.Currency);
                }
                public QuickFix.Fields.IB_EventSeverity IB_EventSeverity
                { 
                    get 
                    {
                        QuickFix.Fields.IB_EventSeverity val = new QuickFix.Fields.IB_EventSeverity();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_EventSeverity val) 
                { 
                    this.IB_EventSeverity = val;
                }
                
                public QuickFix.Fields.IB_EventSeverity Get(QuickFix.Fields.IB_EventSeverity val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_EventSeverity val) 
                { 
                    return IsSetIB_EventSeverity();
                }
                
                public bool IsSetIB_EventSeverity() 
                { 
                    return IsSetField(Tags.IB_EventSeverity);
                }
            
            }
        }
    }
}
