// This is a generated file.  Don't edit it directly!

using QuickFix.Fields;
namespace QuickFix
{
    namespace FIX42 
    {
        public class IB_MarketValue : Message
        {
            public const string MsgType = "RL";

            public IB_MarketValue() : base()
            {
                this.Header.SetField(new QuickFix.Fields.MsgType("RL"));
            }

            public IB_MarketValue(
                    QuickFix.Fields.IB_ReqID aIB_ReqID
                ) : this()
            {
                this.IB_ReqID = aIB_ReqID;
            }

            public QuickFix.Fields.IB_ReqID IB_ReqID
            { 
                get 
                {
                    QuickFix.Fields.IB_ReqID val = new QuickFix.Fields.IB_ReqID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.IB_ReqID val) 
            { 
                this.IB_ReqID = val;
            }
            
            public QuickFix.Fields.IB_ReqID Get(QuickFix.Fields.IB_ReqID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.IB_ReqID val) 
            { 
                return IsSetIB_ReqID();
            }
            
            public bool IsSetIB_ReqID() 
            { 
                return IsSetField(Tags.IB_ReqID);
            }
            public QuickFix.Fields.IB_FirstKey IB_FirstKey
            { 
                get 
                {
                    QuickFix.Fields.IB_FirstKey val = new QuickFix.Fields.IB_FirstKey();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.IB_FirstKey val) 
            { 
                this.IB_FirstKey = val;
            }
            
            public QuickFix.Fields.IB_FirstKey Get(QuickFix.Fields.IB_FirstKey val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.IB_FirstKey val) 
            { 
                return IsSetIB_FirstKey();
            }
            
            public bool IsSetIB_FirstKey() 
            { 
                return IsSetField(Tags.IB_FirstKey);
            }
            public class IB_FirstKeyGroup : Group
            {
                public static int[] fieldOrder = {Tags.IB_SecondKey, Tags.Currency, Tags.IB_CashBalance, Tags.IB_StockMarketValue, Tags.IB_OptionMarketValue, Tags.IB_FutureOptMarketValue, Tags.IB_FuturePnL, Tags.IB_RealizedPnL, Tags.IB_UnrealizedPnL, Tags.IB_TotalCashBalance, Tags.IB_NetLiquidationValue, Tags.IB_ExchangeRate, Tags.IB_NetInterest, Tags.IB_6483, Tags.IB_6681, Tags.IB_6682, Tags.IB_6683, Tags.IB_6684, Tags.IB_6685, Tags.IB_6686, Tags.IB_6687, Tags.IB_6711, Tags.IB_TimeStamp, Tags.IB_EventSeverity, 0};
            
                public IB_FirstKeyGroup() 
                  :base( Tags.IB_FirstKey, Tags.IB_SecondKey, fieldOrder)
                {
                }
            
                public override Group Clone()
                {
                    var clone = new IB_FirstKeyGroup();
                    clone.CopyStateFrom(this);
                    return clone;
                }
            
                public QuickFix.Fields.IB_SecondKey IB_SecondKey
                { 
                    get 
                    {
                        QuickFix.Fields.IB_SecondKey val = new QuickFix.Fields.IB_SecondKey();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_SecondKey val) 
                { 
                    this.IB_SecondKey = val;
                }
                
                public QuickFix.Fields.IB_SecondKey Get(QuickFix.Fields.IB_SecondKey val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_SecondKey val) 
                { 
                    return IsSetIB_SecondKey();
                }
                
                public bool IsSetIB_SecondKey() 
                { 
                    return IsSetField(Tags.IB_SecondKey);
                }
                public QuickFix.Fields.Currency Currency
                { 
                    get 
                    {
                        QuickFix.Fields.Currency val = new QuickFix.Fields.Currency();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.Currency val) 
                { 
                    this.Currency = val;
                }
                
                public QuickFix.Fields.Currency Get(QuickFix.Fields.Currency val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.Currency val) 
                { 
                    return IsSetCurrency();
                }
                
                public bool IsSetCurrency() 
                { 
                    return IsSetField(Tags.Currency);
                }
                public QuickFix.Fields.IB_CashBalance IB_CashBalance
                { 
                    get 
                    {
                        QuickFix.Fields.IB_CashBalance val = new QuickFix.Fields.IB_CashBalance();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_CashBalance val) 
                { 
                    this.IB_CashBalance = val;
                }
                
                public QuickFix.Fields.IB_CashBalance Get(QuickFix.Fields.IB_CashBalance val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_CashBalance val) 
                { 
                    return IsSetIB_CashBalance();
                }
                
                public bool IsSetIB_CashBalance() 
                { 
                    return IsSetField(Tags.IB_CashBalance);
                }
                public QuickFix.Fields.IB_StockMarketValue IB_StockMarketValue
                { 
                    get 
                    {
                        QuickFix.Fields.IB_StockMarketValue val = new QuickFix.Fields.IB_StockMarketValue();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_StockMarketValue val) 
                { 
                    this.IB_StockMarketValue = val;
                }
                
                public QuickFix.Fields.IB_StockMarketValue Get(QuickFix.Fields.IB_StockMarketValue val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_StockMarketValue val) 
                { 
                    return IsSetIB_StockMarketValue();
                }
                
                public bool IsSetIB_StockMarketValue() 
                { 
                    return IsSetField(Tags.IB_StockMarketValue);
                }
                public QuickFix.Fields.IB_OptionMarketValue IB_OptionMarketValue
                { 
                    get 
                    {
                        QuickFix.Fields.IB_OptionMarketValue val = new QuickFix.Fields.IB_OptionMarketValue();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_OptionMarketValue val) 
                { 
                    this.IB_OptionMarketValue = val;
                }
                
                public QuickFix.Fields.IB_OptionMarketValue Get(QuickFix.Fields.IB_OptionMarketValue val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_OptionMarketValue val) 
                { 
                    return IsSetIB_OptionMarketValue();
                }
                
                public bool IsSetIB_OptionMarketValue() 
                { 
                    return IsSetField(Tags.IB_OptionMarketValue);
                }
                public QuickFix.Fields.IB_FutureOptMarketValue IB_FutureOptMarketValue
                { 
                    get 
                    {
                        QuickFix.Fields.IB_FutureOptMarketValue val = new QuickFix.Fields.IB_FutureOptMarketValue();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_FutureOptMarketValue val) 
                { 
                    this.IB_FutureOptMarketValue = val;
                }
                
                public QuickFix.Fields.IB_FutureOptMarketValue Get(QuickFix.Fields.IB_FutureOptMarketValue val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_FutureOptMarketValue val) 
                { 
                    return IsSetIB_FutureOptMarketValue();
                }
                
                public bool IsSetIB_FutureOptMarketValue() 
                { 
                    return IsSetField(Tags.IB_FutureOptMarketValue);
                }
                public QuickFix.Fields.IB_FuturePnL IB_FuturePnL
                { 
                    get 
                    {
                        QuickFix.Fields.IB_FuturePnL val = new QuickFix.Fields.IB_FuturePnL();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_FuturePnL val) 
                { 
                    this.IB_FuturePnL = val;
                }
                
                public QuickFix.Fields.IB_FuturePnL Get(QuickFix.Fields.IB_FuturePnL val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_FuturePnL val) 
                { 
                    return IsSetIB_FuturePnL();
                }
                
                public bool IsSetIB_FuturePnL() 
                { 
                    return IsSetField(Tags.IB_FuturePnL);
                }
                public QuickFix.Fields.IB_RealizedPnL IB_RealizedPnL
                { 
                    get 
                    {
                        QuickFix.Fields.IB_RealizedPnL val = new QuickFix.Fields.IB_RealizedPnL();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_RealizedPnL val) 
                { 
                    this.IB_RealizedPnL = val;
                }
                
                public QuickFix.Fields.IB_RealizedPnL Get(QuickFix.Fields.IB_RealizedPnL val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_RealizedPnL val) 
                { 
                    return IsSetIB_RealizedPnL();
                }
                
                public bool IsSetIB_RealizedPnL() 
                { 
                    return IsSetField(Tags.IB_RealizedPnL);
                }
                public QuickFix.Fields.IB_UnrealizedPnL IB_UnrealizedPnL
                { 
                    get 
                    {
                        QuickFix.Fields.IB_UnrealizedPnL val = new QuickFix.Fields.IB_UnrealizedPnL();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_UnrealizedPnL val) 
                { 
                    this.IB_UnrealizedPnL = val;
                }
                
                public QuickFix.Fields.IB_UnrealizedPnL Get(QuickFix.Fields.IB_UnrealizedPnL val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_UnrealizedPnL val) 
                { 
                    return IsSetIB_UnrealizedPnL();
                }
                
                public bool IsSetIB_UnrealizedPnL() 
                { 
                    return IsSetField(Tags.IB_UnrealizedPnL);
                }
                public QuickFix.Fields.IB_TotalCashBalance IB_TotalCashBalance
                { 
                    get 
                    {
                        QuickFix.Fields.IB_TotalCashBalance val = new QuickFix.Fields.IB_TotalCashBalance();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_TotalCashBalance val) 
                { 
                    this.IB_TotalCashBalance = val;
                }
                
                public QuickFix.Fields.IB_TotalCashBalance Get(QuickFix.Fields.IB_TotalCashBalance val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_TotalCashBalance val) 
                { 
                    return IsSetIB_TotalCashBalance();
                }
                
                public bool IsSetIB_TotalCashBalance() 
                { 
                    return IsSetField(Tags.IB_TotalCashBalance);
                }
                public QuickFix.Fields.IB_NetLiquidationValue IB_NetLiquidationValue
                { 
                    get 
                    {
                        QuickFix.Fields.IB_NetLiquidationValue val = new QuickFix.Fields.IB_NetLiquidationValue();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_NetLiquidationValue val) 
                { 
                    this.IB_NetLiquidationValue = val;
                }
                
                public QuickFix.Fields.IB_NetLiquidationValue Get(QuickFix.Fields.IB_NetLiquidationValue val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_NetLiquidationValue val) 
                { 
                    return IsSetIB_NetLiquidationValue();
                }
                
                public bool IsSetIB_NetLiquidationValue() 
                { 
                    return IsSetField(Tags.IB_NetLiquidationValue);
                }
                public QuickFix.Fields.IB_ExchangeRate IB_ExchangeRate
                { 
                    get 
                    {
                        QuickFix.Fields.IB_ExchangeRate val = new QuickFix.Fields.IB_ExchangeRate();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_ExchangeRate val) 
                { 
                    this.IB_ExchangeRate = val;
                }
                
                public QuickFix.Fields.IB_ExchangeRate Get(QuickFix.Fields.IB_ExchangeRate val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_ExchangeRate val) 
                { 
                    return IsSetIB_ExchangeRate();
                }
                
                public bool IsSetIB_ExchangeRate() 
                { 
                    return IsSetField(Tags.IB_ExchangeRate);
                }
                public QuickFix.Fields.IB_NetInterest IB_NetInterest
                { 
                    get 
                    {
                        QuickFix.Fields.IB_NetInterest val = new QuickFix.Fields.IB_NetInterest();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_NetInterest val) 
                { 
                    this.IB_NetInterest = val;
                }
                
                public QuickFix.Fields.IB_NetInterest Get(QuickFix.Fields.IB_NetInterest val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_NetInterest val) 
                { 
                    return IsSetIB_NetInterest();
                }
                
                public bool IsSetIB_NetInterest() 
                { 
                    return IsSetField(Tags.IB_NetInterest);
                }
                public QuickFix.Fields.IB_6483 IB_6483
                { 
                    get 
                    {
                        QuickFix.Fields.IB_6483 val = new QuickFix.Fields.IB_6483();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_6483 val) 
                { 
                    this.IB_6483 = val;
                }
                
                public QuickFix.Fields.IB_6483 Get(QuickFix.Fields.IB_6483 val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_6483 val) 
                { 
                    return IsSetIB_6483();
                }
                
                public bool IsSetIB_6483() 
                { 
                    return IsSetField(Tags.IB_6483);
                }
                public QuickFix.Fields.IB_6681 IB_6681
                { 
                    get 
                    {
                        QuickFix.Fields.IB_6681 val = new QuickFix.Fields.IB_6681();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_6681 val) 
                { 
                    this.IB_6681 = val;
                }
                
                public QuickFix.Fields.IB_6681 Get(QuickFix.Fields.IB_6681 val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_6681 val) 
                { 
                    return IsSetIB_6681();
                }
                
                public bool IsSetIB_6681() 
                { 
                    return IsSetField(Tags.IB_6681);
                }
                public QuickFix.Fields.IB_6682 IB_6682
                { 
                    get 
                    {
                        QuickFix.Fields.IB_6682 val = new QuickFix.Fields.IB_6682();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_6682 val) 
                { 
                    this.IB_6682 = val;
                }
                
                public QuickFix.Fields.IB_6682 Get(QuickFix.Fields.IB_6682 val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_6682 val) 
                { 
                    return IsSetIB_6682();
                }
                
                public bool IsSetIB_6682() 
                { 
                    return IsSetField(Tags.IB_6682);
                }
                public QuickFix.Fields.IB_6683 IB_6683
                { 
                    get 
                    {
                        QuickFix.Fields.IB_6683 val = new QuickFix.Fields.IB_6683();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_6683 val) 
                { 
                    this.IB_6683 = val;
                }
                
                public QuickFix.Fields.IB_6683 Get(QuickFix.Fields.IB_6683 val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_6683 val) 
                { 
                    return IsSetIB_6683();
                }
                
                public bool IsSetIB_6683() 
                { 
                    return IsSetField(Tags.IB_6683);
                }
                public QuickFix.Fields.IB_6684 IB_6684
                { 
                    get 
                    {
                        QuickFix.Fields.IB_6684 val = new QuickFix.Fields.IB_6684();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_6684 val) 
                { 
                    this.IB_6684 = val;
                }
                
                public QuickFix.Fields.IB_6684 Get(QuickFix.Fields.IB_6684 val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_6684 val) 
                { 
                    return IsSetIB_6684();
                }
                
                public bool IsSetIB_6684() 
                { 
                    return IsSetField(Tags.IB_6684);
                }
                public QuickFix.Fields.IB_6685 IB_6685
                { 
                    get 
                    {
                        QuickFix.Fields.IB_6685 val = new QuickFix.Fields.IB_6685();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_6685 val) 
                { 
                    this.IB_6685 = val;
                }
                
                public QuickFix.Fields.IB_6685 Get(QuickFix.Fields.IB_6685 val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_6685 val) 
                { 
                    return IsSetIB_6685();
                }
                
                public bool IsSetIB_6685() 
                { 
                    return IsSetField(Tags.IB_6685);
                }
                public QuickFix.Fields.IB_6686 IB_6686
                { 
                    get 
                    {
                        QuickFix.Fields.IB_6686 val = new QuickFix.Fields.IB_6686();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_6686 val) 
                { 
                    this.IB_6686 = val;
                }
                
                public QuickFix.Fields.IB_6686 Get(QuickFix.Fields.IB_6686 val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_6686 val) 
                { 
                    return IsSetIB_6686();
                }
                
                public bool IsSetIB_6686() 
                { 
                    return IsSetField(Tags.IB_6686);
                }
                public QuickFix.Fields.IB_6687 IB_6687
                { 
                    get 
                    {
                        QuickFix.Fields.IB_6687 val = new QuickFix.Fields.IB_6687();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_6687 val) 
                { 
                    this.IB_6687 = val;
                }
                
                public QuickFix.Fields.IB_6687 Get(QuickFix.Fields.IB_6687 val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_6687 val) 
                { 
                    return IsSetIB_6687();
                }
                
                public bool IsSetIB_6687() 
                { 
                    return IsSetField(Tags.IB_6687);
                }
                public QuickFix.Fields.IB_6711 IB_6711
                { 
                    get 
                    {
                        QuickFix.Fields.IB_6711 val = new QuickFix.Fields.IB_6711();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_6711 val) 
                { 
                    this.IB_6711 = val;
                }
                
                public QuickFix.Fields.IB_6711 Get(QuickFix.Fields.IB_6711 val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_6711 val) 
                { 
                    return IsSetIB_6711();
                }
                
                public bool IsSetIB_6711() 
                { 
                    return IsSetField(Tags.IB_6711);
                }
                public QuickFix.Fields.IB_TimeStamp IB_TimeStamp
                { 
                    get 
                    {
                        QuickFix.Fields.IB_TimeStamp val = new QuickFix.Fields.IB_TimeStamp();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_TimeStamp val) 
                { 
                    this.IB_TimeStamp = val;
                }
                
                public QuickFix.Fields.IB_TimeStamp Get(QuickFix.Fields.IB_TimeStamp val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_TimeStamp val) 
                { 
                    return IsSetIB_TimeStamp();
                }
                
                public bool IsSetIB_TimeStamp() 
                { 
                    return IsSetField(Tags.IB_TimeStamp);
                }
                public QuickFix.Fields.IB_EventSeverity IB_EventSeverity
                { 
                    get 
                    {
                        QuickFix.Fields.IB_EventSeverity val = new QuickFix.Fields.IB_EventSeverity();
                        GetField(val);
                        return val;
                    }
                    set { SetField(value); }
                }
                
                public void Set(QuickFix.Fields.IB_EventSeverity val) 
                { 
                    this.IB_EventSeverity = val;
                }
                
                public QuickFix.Fields.IB_EventSeverity Get(QuickFix.Fields.IB_EventSeverity val) 
                { 
                    GetField(val);
                    return val;
                }
                
                public bool IsSet(QuickFix.Fields.IB_EventSeverity val) 
                { 
                    return IsSetIB_EventSeverity();
                }
                
                public bool IsSetIB_EventSeverity() 
                { 
                    return IsSetField(Tags.IB_EventSeverity);
                }
            
            }
        }
    }
}
