// This is a generated file.  Don't edit it directly!

using QuickFix.Fields;
namespace QuickFix
{
    namespace FIX42 
    {
        public class CQG_CollateralInquiry : Message
        {
            public const string MsgType = "UBB";

            public CQG_CollateralInquiry() : base()
            {
                this.Header.SetField(new QuickFix.Fields.MsgType("UBB"));
            }

            public CQG_CollateralInquiry(
                    QuickFix.Fields.CQG_CollInquiryID aCQG_CollInquiryID
                ) : this()
            {
                this.CQG_CollInquiryID = aCQG_CollInquiryID;
            }

            public QuickFix.Fields.CQG_CollInquiryID CQG_CollInquiryID
            { 
                get 
                {
                    QuickFix.Fields.CQG_CollInquiryID val = new QuickFix.Fields.CQG_CollInquiryID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.CQG_CollInquiryID val) 
            { 
                this.CQG_CollInquiryID = val;
            }
            
            public QuickFix.Fields.CQG_CollInquiryID Get(QuickFix.Fields.CQG_CollInquiryID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.CQG_CollInquiryID val) 
            { 
                return IsSetCQG_CollInquiryID();
            }
            
            public bool IsSetCQG_CollInquiryID() 
            { 
                return IsSetField(Tags.CQG_CollInquiryID);
            }
            public QuickFix.Fields.Account Account
            { 
                get 
                {
                    QuickFix.Fields.Account val = new QuickFix.Fields.Account();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.Account val) 
            { 
                this.Account = val;
            }
            
            public QuickFix.Fields.Account Get(QuickFix.Fields.Account val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.Account val) 
            { 
                return IsSetAccount();
            }
            
            public bool IsSetAccount() 
            { 
                return IsSetField(Tags.Account);
            }
        }
    }
}
