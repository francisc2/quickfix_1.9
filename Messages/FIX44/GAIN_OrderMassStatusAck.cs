// This is a generated file.  Don't edit it directly!

using QuickFix.Fields;
namespace QuickFix
{
    namespace FIX44 
    {
        public class GAIN_OrderMassStatusAck : Message
        {
            public const string MsgType = "BR";

            public GAIN_OrderMassStatusAck() : base()
            {
                this.Header.SetField(new QuickFix.Fields.MsgType("BR"));
            }

            public GAIN_OrderMassStatusAck(
                    QuickFix.Fields.GAIN_MassStatusReqResult aGAIN_MassStatusReqResult
                ) : this()
            {
                this.GAIN_MassStatusReqResult = aGAIN_MassStatusReqResult;
            }

            public QuickFix.Fields.MassStatusReqID MassStatusReqID
            { 
                get 
                {
                    QuickFix.Fields.MassStatusReqID val = new QuickFix.Fields.MassStatusReqID();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.MassStatusReqID val) 
            { 
                this.MassStatusReqID = val;
            }
            
            public QuickFix.Fields.MassStatusReqID Get(QuickFix.Fields.MassStatusReqID val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.MassStatusReqID val) 
            { 
                return IsSetMassStatusReqID();
            }
            
            public bool IsSetMassStatusReqID() 
            { 
                return IsSetField(Tags.MassStatusReqID);
            }
            public QuickFix.Fields.GAIN_MassStatusReqResult GAIN_MassStatusReqResult
            { 
                get 
                {
                    QuickFix.Fields.GAIN_MassStatusReqResult val = new QuickFix.Fields.GAIN_MassStatusReqResult();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.GAIN_MassStatusReqResult val) 
            { 
                this.GAIN_MassStatusReqResult = val;
            }
            
            public QuickFix.Fields.GAIN_MassStatusReqResult Get(QuickFix.Fields.GAIN_MassStatusReqResult val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.GAIN_MassStatusReqResult val) 
            { 
                return IsSetGAIN_MassStatusReqResult();
            }
            
            public bool IsSetGAIN_MassStatusReqResult() 
            { 
                return IsSetField(Tags.GAIN_MassStatusReqResult);
            }
            public QuickFix.Fields.Text Text
            { 
                get 
                {
                    QuickFix.Fields.Text val = new QuickFix.Fields.Text();
                    GetField(val);
                    return val;
                }
                set { SetField(value); }
            }
            
            public void Set(QuickFix.Fields.Text val) 
            { 
                this.Text = val;
            }
            
            public QuickFix.Fields.Text Get(QuickFix.Fields.Text val) 
            { 
                GetField(val);
                return val;
            }
            
            public bool IsSet(QuickFix.Fields.Text val) 
            { 
                return IsSetText();
            }
            
            public bool IsSetText() 
            { 
                return IsSetField(Tags.Text);
            }
        }
    }
}
